<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CallhistoryController
 * 
 * @author Prabath
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\cdr;

class CallhistoryController extends Controller {
    
    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Sandun
     * @since 2017-09-25
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
//        if (!Yii::$app->session->has('user_id')) {
//            $this->redirect('index.php?r=user/login_view');
//        }        
    }

    
    public function actionCallhistory() {
        $callHistory = cdr::getCallHistory();
        $json_callHistory = json_encode($callHistory);
        return $this->render('callhistoryPage', ['callHistory' => $json_callHistory]);
    }

}
