<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\Call_records;
use app\models\agent_notifications;
use app\models\Contact_list;
use app\models\call_center_user;
use app\models\Call_comments;

/**
 * This controller is to provide functions for the Agents functionalities
 *
 * @author Sandun
 * @since 2017-06-27
 */
class AgentController extends Controller {
    
    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Sandun
     * @since 2017-09-25
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
//        if (!Yii::$app->session->has('user_id')) {
//            $this->redirect('index.php?r=user/login_view');
//        }        
    }

    /**
     * <b>Render the call coordinator page</b>
     * <p>This function renders the agent call information interface, if there is a value for the GET variable `customer`, the data of the passed customer id will be fetched from the database.
     * If the GET `customer` variable not exist, by default customer #1 information will be shown </p>
     * 
     * @return render the call coordinator page
     * 
     * @author Sandun
     * @since 2017-06-27
     * 
     */
    public function actionCallerinformation() {
        $session = Yii::$app->session;
        if (!$session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        } else {
            $webphoneParams = require(__DIR__ . '/../config/webphone.php');
            $webphoneParams['username'] = Yii::$app->session->get('voip');
            $webphoneParams['phoneState'] = call_center_user::getAgentWebphoneState($session->get('user_id'));
            $contacts = $this->allcontacts();
            $agentId = Yii::$app->session->get("user_id");
            $consumedAgentRequest = Agent_requests::getConsumedAgentRequests($agentId);
            $requestData = array();
            $i = 0;
            foreach ($consumedAgentRequest as $key) {
//                $type = $key['request_type'];
                $requestData[$i]['type'] = $key['request_type'];
                $requestData[$i]['id'] = $key['id'];
                $requestData[$i]['status'] = $key['request_status'];
                $requestData[$i]['time'] = $key['approved_time_period'];
                $i++;
            }

            if (isset($_GET['customer'])) {
                $customerId = $_GET['customer'];
                $customerInformation = Customer::getCustomerInformation($customerId);

                return $this->render('callCoordinator', ['customerInformation' => $customerInformation, 'webphoneParams' => $webphoneParams, 'contacts' => $contacts, 'agent_request_onload' => $requestData]);
            } else {
                $customerInformation = Customer::getCustomerInformation(1);
                return $this->render('callCoordinator', ['customerInformation' => $customerInformation, 'webphoneParams' => $webphoneParams, 'contacts' => $contacts, 'agent_request_onload' => $requestData]);
            }
//            } else {
//                $this->redirect('index.php?r=user/login_view');
//            }
        }
    }

    /**
     * <b>Render output customer details</b>
     * <p>This function returns customer details of the answered phone call</p>
     * 
     * @return array of customer details
     * 
     * @author Vikum
     * @since 2017-07-07
     * 
     */
    public function actionCustomerdetails() {
        $customerNumber = $_GET['q'];
        $customerInfo = Customer::getCustomerInformationByPhoneNumber($customerNumber);


        // piece1
        if (count($customerInfo) > 0) {
            $fullDate = $customerInfo[0]['dob'];
            $pieces = explode(" ", $fullDate);
            $customerData = array(
                'customer_name' => $customerInfo[0]['customer_name'],
                'contact' => $customerInfo[0]['contact_number'],
                'address' => $customerInfo[0]['address'],
            );
            $customerCallData = cdr::getRecordsByNumber($customerNumber);
            $customerData['callRecords'] = $customerCallData;
        } else {
            $customerData = array(
                'customer_name' => '',
                'email' => '',
                'contact' => '',
                'address' => '',
                'dob' => '',
                'gender' => 'male'
            );
            $customerData['callRecords'] = array();
        }


        echo json_encode($customerData);
    }

    /**
     * <b>Gets the agentId and pass it to the model to be saved</b>
     * <p>this function captures by a GET varaible the agentId of the agent who made the help request and pass it to the Help_requests model to be saved</p>
     * 
     * @print bool
     * @since 2017-07-13
     * @author Sandun
     */
    public function actionRequesthelp() {
        $agentId = Yii::$app->session->get("user_id");
        if (Help_requests::saveRequest($agentId)) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function actionRequestbreak() {
        $agentId = Yii::$app->session->get("user_id");
        $requestType = $_POST['request_type'];
        $requestStatus = 'pending';
        $requestData = array(
            'request_type' => $requestType,
            'request_status' => $requestStatus,
            'agent_id' => $agentId
        );
        if (Agent_requests::saveRequest($requestData)) {
            echo '1';
        } else {
            echo '0';
        }
    }

    /*
     * This function will record agent taking the break
     * @author: Vikum
     * @since: 20/07/2017
     *     
     */

    public function actionAgenttakebreak() {
        $requestId = $_POST['q'];
        $requestType = Agent_requests::getAgentRequest($requestId);
        $data = array(
            'taken_time' => date("Y-m-d H:i:s"),
            'request_status' => 'taken',
            'consumed' => 1
        );
        $requestResponce = Agent_requests::setAgentRequestResponse($data, $requestId);
        if ($requestResponce) {
            return 1;
        } else {
            return 0;
        }
    }

    /*
     * This function will ignore the late arrival of an agent from supervisor page. 
     * @author: Prabath
     * @since: 21/07/2017
     *     
     */

    public function actionUpdateagentrequest() {
        Agent_requests::updateLateArrivalRecord($_POST['agent_id']);
    }

    /*
     * This function will record call record in db
     * @author: Vikum
     * @since: 01/08/2017   
     * @depreciated since 2017-09-27 Sandun  
     */

    public function actionSavecallrecord() {
        $number = $_POST['callerNumber'];
        $agentId = Yii::$app->session->get("user_id");
        $time = microtime(true);
        $data = array(
            'call_date' => date("Y-m-d H:i:s"),
            'call_time' => date("Y-m-d H:i:s"),
            'caller_number' => $number,
            'cli_number' => $number,
            'user_id' => $agentId,
            'active' => 1,
            'created_date_time' => date("Y-m-d H:i:s."),
            'timestamp' => $time
        );
        $insert = Call_records::insertNewCallRecord($data);
        if ($insert) {
            echo $insert;
        } else {
            echo 0;
        }
    }

    /**
     * <b>Gets customer information from the HNB API</b>
     * <p>This function sends the customer phone number or the vehicle number </p>
     * 
     * @echo json string of customer information
     * @since 2017-08-17
     * @author Sandun
     * @modified Sandun 2017-09-27
     * @description added the previous calls data retrieval part to no customer data block to show them even if the customer api is unavailable
     */
    public function actionGetcustomerinformation() {
//        $number = $_GET['number'];
        $number = $_POST['number'];
        $result = $this->Search_customer_data($number);
        $data = json_decode($result, TRUE);
        if (count($data['content']) > 0) {

            if (count($data['content']) == 1) {
                // has only 1 policy
                $result_array = array(
                    'customer_policyNumber' => $data['content'][0]['policyNo'],
                    'customer_insuered_name' => $data['content'][0]['insuredName'],
                    'customer_address' => $data['content'][0]['insuredAddress'],
                    'customer_contact' => $data['content'][0]['contactNo'],
                    'customer_NIC' => $data['content'][0]['insuredhNic'],
                    'customer_effectiveDate' => $data['content'][0]['effectiveDate'],
                    'customer_expiryDate' => $data['content'][0]['expiryDate'],
                    'customer_policyType' => $data['content'][0]['policyType'],
                    'customer_policyStatus' => $data['content'][0]['policyStatus'],
                    'customer_totalPremium' => $data['content'][0]['policyPremium'], // This has to confirm
                    'customer_isVIP' => $data['content'][0]['vipFlag'],
                    'customer_policies' => 1, // This is to idicate has only 1 policy
                    'policy_vehicleNumber' => $data['content'][0]['vehicleNo'],
                    'policy_sumInsured' => $data['content'][0]['sumInsured'],
                    'policy_engineNumber' => $data['content'][0]['engineNo'],
                    'policy_chasisNumber' => $data['content'][0]['chassiNo'],
                    'policy_vehicleCategory' => $data['content'][0]['vehicleCatagory'],
                    'policy_cubicCapacity' => $data['content'][0]['cubicCapacity'],
                    'policy_yearOfMake' => $data['content'][0]['makeYear'],
                    'policy_make' => $data['content'][0]['vehicleMake'],
                    'policy_model' => $data['content'][0]['vehicleModel'],
                    'policy_vehicleUsage' => $data['content'][0]['vehicleUsage'],
                );

                $customerCallData = $this->getCustomerCallDataWithComments($number); //cdr::getRecordsByNumber($number);
                $result_array['callRecords'] = $customerCallData;
            } else {
                // has more than 1 policy

                $result_array = array(
                    'customer_policyNumber' => "",
                    'customer_insuered_name' => $data['content'][0]['insuredName'],
                    'customer_address' => $data['content'][0]['insuredAddress'],
                    'customer_contact' => $data['content'][0]['contactNo'],
                    'customer_NIC' => $data['content'][0]['insuredhNic'],
                    'customer_effectiveDate' => $data['content'][0]['effectiveDate'],
                    'customer_expiryDate' => $data['content'][0]['expiryDate'],
                    'customer_policyType' => $data['content'][0]['policyType'],
                    'customer_policyStatus' => $data['content'][0]['policyStatus'],
                    'customer_totalPremium' => $data['content'][0]['policyPremium'], // This has to confirm
                    'customer_isVIP' => $data['content'][0]['vipFlag'],
                    'customer_policies' => count($data['content']) // This is to idicate has more than 1 policy
                );

                $customerCallData = $this->getCustomerCallDataWithComments($number); //cdr::getRecordsByNumber($number);
                $result_array['callRecords'] = $customerCallData;

                $vehicleNumbersArray = array();
                for ($x = 0; $x < count($data['content']); $x++) {
                    $vehicleDetailsArray = array(
                        'customer_policyNumber' => $data['content'][$x]['policyNo'],
                        'customer_insuered_name' => $data['content'][$x]['insuredName'],
                        'customer_address' => $data['content'][$x]['insuredAddress'],
                        'customer_contact' => $data['content'][$x]['contactNo'],
                        'customer_NIC' => $data['content'][$x]['insuredhNic'],
                        'customer_effectiveDate' => $data['content'][$x]['effectiveDate'],
                        'customer_expiryDate' => $data['content'][$x]['expiryDate'],
                        'customer_policyType' => $data['content'][$x]['policyType'],
                        'customer_policyStatus' => $data['content'][$x]['policyStatus'],
                        'customer_totalPremium' => $data['content'][$x]['policyPremium'], // This has to confirm
                        'customer_isVIP' => $data['content'][$x]['vipFlag'],
                        'policy_vehicleNumber' => $data['content'][$x]['vehicleNo'],
                        'policy_sumInsured' => $data['content'][$x]['sumInsured'],
                        'policy_engineNumber' => $data['content'][$x]['engineNo'],
                        'policy_chasisNumber' => $data['content'][$x]['chassiNo'],
                        'policy_vehicleCategory' => $data['content'][$x]['vehicleCatagory'],
                        'policy_cubicCapacity' => $data['content'][$x]['cubicCapacity'],
                        'policy_yearOfMake' => $data['content'][$x]['makeYear'],
                        'policy_make' => $data['content'][$x]['vehicleMake'],
                        'policy_model' => $data['content'][$x]['vehicleModel'],
                        'policy_vehicleUsage' => $data['content'][$x]['vehicleUsage'],                        
                    );
                    array_push($vehicleNumbersArray, $vehicleDetailsArray);
                }
                $result_array['vehicleNumbers'] = $vehicleNumbersArray;
            }
        } else {
            $result_array = array(
                'customer_policyNumber' => "",
                'customer_insuered_name' => "",
                'customer_address' => "",
                'customer_contact' => "",
                'customer_NIC' => "",
                'customer_effectiveDate' => "",
                'customer_expiryDate' => "",
                'customer_policyType' => "",
                'customer_policyStatus' => "",
                'customer_totalPremium' => "",
                'customer_isVIP' => "",
                'customer_policies' => 0 // This is to idicate has no policies
            );
            
            $customerCallData = $this->getCustomerCallDataWithComments($number); //cdr::getRecordsByNumber($number);
            $result_array['callRecords'] = $customerCallData;
        }

        echo json_encode($result_array);
    }

    public function actionTestcustomerapi() {
        $number = $_GET['number'];
        echo $this->Search_customer_data($number);
    }

    public function actionTestpreviouscalldata() {
        $number = $_GET['number'];
//        set_time_limit(300);
        echo json_encode($this->getCustomerCallDataWithComments($number));
    }

    private function getCustomerCallDataWithComments($number) {
        $callRecordsData = cdr::getAllAnsweredAndMissedCallsOfCaller($number);
        // set comment
        $callRecordsWithCommentsArray = array();
        for ($x = 0; $x < count($callRecordsData); $x++) {
            if (cdr::isThisUniqueIdAMissedCall($callRecordsData[$x]['uniqueid']) == TRUE) {
                
//                if(cdr::isThisUniqueIdAMissedCall($callRecordsData[$x]['uniqueid']) == TRUE){
                    $callRecord = array(
                        "uniqueid" => $callRecordsData[$x]['uniqueid'],
                        "start" => $callRecordsData[$x]['start'],
                        "src" => $callRecordsData[$x]['src'],
                        "dst" => "-",
                        "disposition" => $callRecordsData[$x]['disposition'],
                        "duration" => "-",
                        "status" => "Missed",
                        "comment" => "-"
                    );    
                    
                array_push($callRecordsWithCommentsArray, $callRecord);
                
//                }
                               
                
//                $callRecordsData[$x]['status'] = "Missed";
//                $callRecordsData[$x]['dst'] = "-";
//                $callRecordsData[$x]['comment'] = "-";
            } else {
                $answeredCdrData = cdr::getAnsweredExtensionFromUniqueid($callRecordsData[$x]['uniqueid']);
                $answeredAgentInfo = call_center_user::getUserInfoFromVOIPExtension($answeredCdrData['dst']);
                $comment = Call_comments::getCommentDataFromUniqueId($callRecordsData[$x]['uniqueid']); 
                
                    $callRecord = array(
                    "uniqueid" => $callRecordsData[$x]['uniqueid'],
                    "start" => $answeredCdrData['start'],
                    "src" => $callRecordsData[$x]['src'],
                    "dst" => $answeredAgentInfo['name'],
                    "disposition" => $answeredCdrData['disposition'],
                    "duration" => $answeredCdrData['duration'],
                    "status" => "Answered",
                    "comment" => ($comment['comment'] != NULL ? $comment['comment'] : "-")
                );
                
                array_push($callRecordsWithCommentsArray, $callRecord);
                
//                $callRecordsData[$x]['status'] = "Answered";
//                $answeredAgentInfo = call_center_user::getUserInfoFromVOIPExtension($callRecordsData[$x]['dst']);
//                $callRecordsData[$x]['dst'] = $answeredAgentInfo['name'];
//
//                $comment = Call_comments::getCommentDataFromUniqueId($callRecordsData[$x]['uniqueid']);
//                if($comment){
//                    $callRecordsData[$x]['comment'] = $comment['comment'];
//                }else{
//                    $callRecordsData[$x]['comment'] = "-";
//                }
            }
        }
        return $callRecordsWithCommentsArray;
    }

    /*
     * This function will record call hung up in a call record
     * @author: Vikum
     * @since: 01/08/2017     
     * @depreciated since 2017-09-27 Sandun
     */

    public function actionHungupcallrecord() {
        $id = $_POST['callerId'];
        $callerRecord = Call_records::getCallRecordFormId($id);
        $answeredTimestamp = $callerRecord['timestamp'];
        $callerNumber = $callerRecord['cli_number'];
        $callDuration = microtime(true) - $answeredTimestamp;
//        sleep(2);
        $uniqueId = cdr::getMostRecentUniqueId($callerNumber);
        if ($uniqueId) {
            $fullCallDuration = cdr::getRecordsDurationByUniqueId($uniqueId[0]['uniqueid']);
            $entireDuration = $fullCallDuration[0]['num'] - $callDuration;
            if ($entireDuration < 0) {
                $entireDuration = 0;
            }
            $data = array(
                'cdr_unique_id' => $uniqueId[0]['uniqueid'],
                'call_waiting_duration' => $entireDuration,
                'call_end_time' => date("Y-m-d H:i:s")
            );
            $update = Call_records::updateCallRecord($data, $id);
            if ($update) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }
    
    public function actionGetcalluniqueidonhungup(){
        $callerNumber = $_GET['callerNumber'];
        $hunguptime = time();
        $agentExtension = Yii::$app->session->get("voip");
        
        $count = 0;
        $result = 0;
        while($count < 6){
            $uniqueid = cdr::getUniqueIdByCallerNumberAgentExtensionAndEndTime($callerNumber, $agentExtension, $hunguptime);
            if($uniqueid != NULL){
                $result = $uniqueid;
                break;
            }else{
            	$count++;
            	sleep(1);
        	}
        }
        
        echo $result;
        
    }

    /**
     * <b>Saves the comment for the call</b>
     * <p>This function saves the comment of the answered call</p>
     * @author Sandun
     * @since 2017-09-27
     * 
     */
    public function actionSavecallrecordcomment() {
        $callerNumber = $_POST['callerNumber'];
        $answeredExtension = Yii::$app->session->get("voip");
        $comment = $_POST['commentText'];
        $uniqueId = $_POST['cdrUniqueId'];
        $update = Call_comments::saveComment($uniqueId, $comment, $callerNumber, $answeredExtension);
        if ($update) {
            echo 1;
        } else {
            echo 0;
        }
        
        
        
    }

    /**
     * <b>Save conference call invitation in DB</b>
     * <p>This function saves the conferece call invitation to DB</p>
     * 
     * @return echo 1 / 0
     * 
     * @author Sandun
     * @since 2017-08-04
     * 
     */
    public function actionSendconferencecallinvitation() {
        $notificationData = array(
            'from' => Yii::$app->session->get('voip'),
            'to' => $_GET['to'],
            'notif_type' => 3, // conference call
            'status' => 'active'
        );

        if (agent_notifications::insertAgentNotification($notificationData)) {
            echo "1";
        } else {
            echo "0";
        }
    }

    public function Search_customer_data($val) {
        $result = '-1';
        if (strpos($val, '-') !== false) {
            $result = $this->searchCustomerData('vehicle_number', $val);
        } else if (preg_match("/[a-z]/i", $val)) {
            $result = $this->searchCustomerData('vehicle_number', $val);
//        } else if (preg_match('/^[0-9]+$/', $val)) {
        } else if (preg_match('/^\d{8,10}$/', $val)) {
            $result = $this->searchCustomerData('contact_number', $val);
        } else if (preg_match('/^\+\d/', $val)) {
            $result = $this->searchCustomerData('contact_number', $val);
        }
        return $result;
    }
    
    /**
     * <b>This is to test the customer data api</b>
     * @author Sandun
     * @since 2017-10-17
     */
    public function actionTest_search_customer_data() {
        $val = $_GET['val'];
        $result = '-1';
        if (strpos($val, '-') !== false) {
            $result = $this->searchCustomerData('vehicle_number', $val);
        } else if (preg_match("/[a-z]/i", $val)) {
            $result = $this->searchCustomerData('vehicle_number', $val);
//        } else if (preg_match('/^[0-9]+$/', $val)) {
        } else if (preg_match('/^\d{8,10}$/', $val)) {
            $result = $this->searchCustomerData('contact_number', $val);
        } else if (preg_match('/^\+\d/', $val)) {
            $result = $this->searchCustomerData('contact_number', $val);
        }
        echo $result;
    }

    private function searchCustomerData($type, $value) {
        $logs = require(__DIR__ . '/../config/api_config.php');

        $url = $logs['url'];
        $userName = $logs['user'];
        $password = $logs['passsword'];

        if ($type == 'contact_number') {
            $url = $url . "contactNo=$value&policyStatus=ACTIVE_POLICY";
        } else if ($type == 'vehicle_number') {
            $url = $url . "vehicleNo=$value&policyStatus=ACTIVE_POLICY";
        }
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "Authorization: Basic " . base64_encode("$userName:$password")
            )
        );
        $context = stream_context_create($opts);
        $file = file_get_contents($url, false, $context);
        return $file;
    }

    private function allcontacts() {

        $contactRecord = Contact_list::loadAllContact();
        $returnData = array();
        if ($contactRecord) {
            foreach ($contactRecord as $key) {
                $val = array(
                    'number' => $key['contact_number'],
                    'name' => $key['contact_name']
                );
                array_push($returnData, $val);
            }

            return json_encode($returnData);
        } else {
            return 0;
        }
    }

}
