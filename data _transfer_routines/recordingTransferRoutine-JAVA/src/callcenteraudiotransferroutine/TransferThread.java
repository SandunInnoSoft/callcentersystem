
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * @author Sandun
 * @since 2018-07-10
 */
package callcenteraudiotransferroutine;
import java.lang.Runnable;
import java.lang.Thread;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import callcenteraudiotransferroutine.TransferByHour;
import callcentercdrflagger.CdrFlagger;

/**
 *<b>This class manages the timely scheduled executions of the routines</b>
 *
 * @author Sandun
 * @since 2018-07-10
 */
public class TransferThread {
    
  private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

  /**
   * <b>Routines schedule function</b>
   * 
   * @author Sandun
   * @since 2018-07-10
   */
   public void routinesSchedule() {
	 final TransferConfigurations transferConfig = new TransferConfigurations();
	 
     final Runnable mainRoutine = new Runnable() {
       public void run() { 
    	   
    	   String routineStartedTime = LocalDateTime.now().toString();
    	   
    	   System.out.println("Initiating transfer called");
    	   int numberOfAudioFilesCopied = TransferByHour.initiateTransfer();
    	   
           System.out.println("Initiating audio file search for misses");
           int missedFilesCount = CdrFlagger.flagLastHourRecords();
           
           System.out.println("Transfer and flagging completed");
           System.out.println("Next execution starts in "+transferConfig.getRoutineSleepMinutes()+" minutes at "+new Date(System.currentTimeMillis() + 1800*1000).toString());
       
           TransferExecutionLog transferLogObj = new TransferExecutionLog();
           transferLogObj.commitTransferExecutionLog(numberOfAudioFilesCopied, routineStartedTime, LocalDateTime.now().toString(), missedFilesCount);
           
       }
     };
     final ScheduledFuture<?> mainRoutineHandle = scheduler.scheduleAtFixedRate(mainRoutine, 0, transferConfig.getRoutineSleepMinutes(), TimeUnit.MINUTES);
   }
    
}
