package callcenteremail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import callcenteraudiotransferroutine.TransferConfigurations;

public class ExtensionsUtil {
	
	private ArrayList<Map> extensionQueues = new ArrayList<Map>(); 
	private ArrayList<Map> callChannels = new ArrayList<Map>(); 

	public ExtensionsUtil() {
        TransferConfigurations config = new TransferConfigurations();
        
        String ipAddress = config.getMainDbIpAddress();
        Integer port = config.getMainDbPort();
        String database = config.getMainDbName();
        String username = config.getMainDbUser();
        String password = config.getMainDbPassword();
        
        String connectionString = "jdbc:mysql://"+ipAddress+":"+port+"/"+database;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(connectionString, username, password);
            
            Statement extensionQueueStatement = con.createStatement();
            Statement callChannelsStatement = con.createStatement();
            
            String extensionQueuesQuery = "SELECT extension_queue.id AS queueId, extension_queue.`name` AS queueName, ext_queue_intermediate.voip_extension AS extension " 
            		+"FROM extension_queue, ext_queue_intermediate "
            		+"WHERE ext_queue_intermediate.queueid = extension_queue.id "
            		+"ORDER BY ext_queue_intermediate.voip_extension ASC";
            
            String callChannelsQuery = "SELECT channel_number, channel_name "
            		+"FROM call_channel "
            		+"ORDER BY channel_number ASC";
            
//            System.out.println("Extensions query - "+extensionQueuesQuery);
//            System.out.println("call channels query "+callChannelsQuery);
            
            ResultSet extensionQueuesResult = extensionQueueStatement.executeQuery(extensionQueuesQuery);
            ResultSet channelsResult = callChannelsStatement.executeQuery(callChannelsQuery);
            
            while(extensionQueuesResult.next()) {
            	Map extensionQueueData = new HashMap();
            	extensionQueueData.put("queueId", extensionQueuesResult.getInt("queueId"));
            	extensionQueueData.put("queueName", extensionQueuesResult.getString("queueName"));
            	extensionQueueData.put("extension", extensionQueuesResult.getInt("extension"));
            	
            	this.extensionQueues.add(extensionQueueData);
            }
            
            while(channelsResult.next()) {
            	Map channelData = new HashMap();
            	channelData.put("channel_name", channelsResult.getString("channel_name"));
            	channelData.put("channel_number", channelsResult.getString("channel_number"));
            	
            	this.callChannels.add(channelData);
            }
            
        }catch(Exception e) {
        	System.out.println("Exception occurred at constructor extensions util");
        	e.printStackTrace();
        }
	}
	
	public String getQueueNameOfExtension(String extension) {
		Map extensionQueueData =  null;
		String matchingQueueName = null;
		for(int x = 0; x < this.extensionQueues.size(); x++) {
			matchingQueueName = null;
			extensionQueueData  = this.extensionQueues.get(x);
			if(extensionQueueData.get("extension").toString().matches(extension)) {
				// has a match
				matchingQueueName = extensionQueueData.get("queueName").toString();
				break;
			}
		}
		return matchingQueueName;
	}
	
	public String getChannelName(String channelNumber) {
		Map channelData =  null;
		String matchingChannelName = null;
		for(int x = 0; x < this.callChannels.size(); x++) {
			matchingChannelName = null;
			channelData  = this.callChannels.get(x);
			if(channelData.get("channel_number").toString().matches(channelNumber)) {
				// has a match
				matchingChannelName = channelData.get("channel_name").toString();
				break;
			}
		}
		return matchingChannelName;
	}
	
	public static void main(String[] args) {
		ExtensionsUtil extensionsUtil = new ExtensionsUtil();
		System.out.println(extensionsUtil.getQueueNameOfExtension("3191"));;
	}
	
}
