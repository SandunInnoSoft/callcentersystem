# HNB call center database update version 1.8
# @since 2017-07-12
# @author Sandun

-- This deletes the call_event_data table
DROP TABLE `call_event_data`;

-- This creates a new table to keep the help requests made by agents
CREATE TABLE `help_requests` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `agentId` INT NULL,
  `datetime` DATETIME NULL,
  PRIMARY KEY (`id`))
COMMENT = 'This table records the help requests made by agents';
