/**
* HNB call center database update version 4.5
* @author Sandun
* @since 2019-01-18
* @target logs DB
*/

-- adds a new table to record the call audio transfer routine execution data

CREATE TABLE `audioTransferRoutineLog` (
`id`  int NOT NULL AUTO_INCREMENT ,
`copiedFilesCount`  int NOT NULL ,
`executionStartedTime`  datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP ,
`executionEndedTime`  datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`)
)
;

