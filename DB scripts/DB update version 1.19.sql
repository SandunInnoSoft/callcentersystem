# this table will Delete unwanted columns from call_records table
ALTER TABLE `call_records` 
DROP COLUMN `customer_id`,
DROP COLUMN `who_is_editing`,
DROP COLUMN `call_status`;


ALTER TABLE `call_records` 
DROP COLUMN `case_id`,
CHANGE COLUMN `cdr_id` `cdr_unique_id` INT(11) NULL DEFAULT NULL ;
