# HNB GENERAL DB UPDATE VERSION 4.1
# @AUTHOR SANDUN
# @SINCE 2018-1-16
# @target DB1 hnbgeneraldb

-- This script creates a new table to maintain the channels information of the extension queues
-- These channels are identified in the PBX as call queues
CREATE TABLE `call_channel` (
`id` int NOT NULL AUTO_INCREMENT ,
`channel_number`  int NULL ,
`channel_description`  varchar(255) NULL ,
`channel_name`  varchar(255) NULL ,
PRIMARY KEY (`id`)
)
COMMENT='This table is to maintain the channels associated with extensions queues. The channels listed here are identified in the PBX as call queues'
;

-- makes the channel_number field unique
ALTER TABLE `call_channel`
ADD UNIQUE (`channel_number`);

-- This creates another new table to maintain the mapping of the call channels with its related extension queue
CREATE TABLE `extqueue_callchannel_intermediate` (
`id`  int NOT NULL AUTO_INCREMENT ,
`channel_id`  int NULL ,
`extqueue_id`  int NULL ,
PRIMARY KEY (`id`)
)
COMMENT='This table is to keep the mapping of the call channels with the related extension queue'
;

