<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;

//if ($customerInformation[0]['gender'] == 'male' || $customerInformation[0]['gender'] == 1) {
// customer is male
$profileImageSrc = "images/dummy_profile_male.jpg";
//} else {
// customer is female
//    $profileImageSrc = "images/dummy_profile_female.jpg";
//}

if (isset($_GET['late'])) {
    $late = 1;
} else {
    $late = 0;
}
?>

<script src="js/fontawesomeGlymphycons.js"></script>
<style>

    body{
        background-color: #ededed;  
        font-size: 10px;
    }            
    .fontSizeClass{
        font-size: 1em;
        margin-bottom: 2px;
    }

    .fieldHeader{
        color: #10297d;
    }

    h3{
        color: #10297d;
    }

    .table-striped>tbody>tr:nth-child(odd)>td, 
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #e3eef7; /* Choose your own color here */
    }
    .table-striped>tbody>tr:nth-child(even)>th {
        background-color: #fff; /* Choose your own color here */
    }  

    a{
        cursor: pointer;
    }
    .disabled {
        pointer-events: none;
        cursor: default;
    }
</style>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--> 
<style>
    .fontSizeClass{
        font-size: 1em;
        margin-bottom: 0.5%;
    }

    body{
        background-color: #ededed;                
    }
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>
<style>
    @import url(css/font1.css);
    @import url(css/font-awesome.min.css);

    .span4
    {
        width: 35px;
        float: left;
        margin: 0 4px 8px 8px;
    }

    .phone
    {
        /*padding-top: 15px;*/
        /*padding-bottom: 10px;*/
        background: #fff;
        border: 1px solid #333;
        border-radius: 5px;
    }
    .tel
    {
        font-family: 'Lato' , sans-serif;   
        font-weight: bold;
        font-size: 20px;
        margin-bottom: 10px;
        margin-top: 10px;
        border: 1px solid #9e9e9e;
        border-radius: 5px;
    }
    .num-pad
    {
        padding-left: 10px;
    }

    .num
    {
        border: 1px solid #9e9e9e;
        -webkit-border-radius: 999px;
        border-radius: 999px;
        -moz-border-radius: 999px;
        height: 35px;
        background-color: #fff;
        color: #333;
        cursor: pointer;
    }
    .num:hover
    {
        background-color: #10297d;
        color: #faa61a;
        transition-property: background-color .2s linear 0s;
        -moz-transition: background-color .2s linear 0s;
        -webkit-transition: background-color .2s linear 0s;
        -o-transition: background-color .2s linear 0s;
    }
    .txt
    {
        font-size: 15px;
        font-weight: bold;
        text-align: center;
        margin-top: 2px;
        font-family: 'Lato' , sans-serif;
        line-height: 30px;
        color: #333;
    }
    .txt:hover{
        color: #faa61a;                
    }
    .small
    {
        font-size: 15px;
    }
    .btn
    {
        font-family: 'Lato' , sans-serif;        
        margin-top: 8px;
        font-weight: bold;
        -webkit-transition: .1s ease-in background-color;
        -webkit-font-smoothing: antialiased;
        letter-spacing: 1px;
    }
    .btn:hover
    {
        transition-property: background-color .2s linear 0s;
        -moz-transition: background-color .2s linear 0s;
        -webkit-transition: background-color .2s linear 0s;
        -o-transition: background-color .2s linear 0s;
    }
    .spanicons
    {
        width: 48px;
        float: left;
        text-align: center;
        margin-top: 10px;
        color: #000;
        font-size: 20px;
        cursor: pointer;
    }
    .spanicons:hover
    {
        color: #faa61a;
        transition-property: color .2s linear 0s;
        -moz-transition: color .2s linear 0s;
        -webkit-transition: color .2s linear 0s;
        -o-transition: color .2s linear 0s;
    }
    .keypad:hover{
        color: #faa61a;
        transition-property: color .2s linear 0s;
        -moz-transition: color .2s linear 0s;
        -webkit-transition: color .2s linear 0s;
        -o-transition: color .2s linear 0s;                
    }
    .badge{
        background-color: rgba(119, 119, 119, 0);
    }
    .list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
        background-color: #10297d;
        border-color: #10297d;
    }            
    .onlineTime{
        font-size: 10px;
    }

    #callerHistoryTable > td {
        text-align: center;
    }
    #idCustomerName{
        font-size: 13px;
    }


</style>

<script src='notifit/notifIt.js'></script>
<link rel='stylesheet' href='notifit/notifIt.css'>
<?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 
    <script>
        // for DND mode functions
        //    $(document).ready(function () {
        //        $("[name='dndSwitch']").bootstrapSwitch();
        //    });

        //    $(function () {
        //        $("#dndSwitch").bootstrapSwitch();
        //    });
        //        var onloadAgentRequests = new Array();
        var webphoneRegistered = false;
        var userRole = "super_agent";
        var isDNDOn = false;
        var contactNamesArray = new Array(); // This array hold all contact names in the contact list
        var interval; // This keeps the JS interval object which runs on every 1 second since the page loaded until the webphone is registered 
        var onloadAgentRequests = new Array();
    <?php
    if (count($agentRequests) > 0) {
        $i = 0;
        foreach ($agentRequests as $key) {
            ?>
                var data = ["<?= $key['type'] ?>", "<?= $key['id'] ?>", "<?= $key['name'] ?>"];
                onloadAgentRequests.push(data);
            <?php
            $i++;
        }
    }
    ?>
        function turnDndOn() {
            //            var number = "*74";
            document.getElementById('webphoneframe').contentWindow.unregisterWebphone();
            // document.getElementById('webphoneframe').contentWindow.setPhoneDndOn();
            $("#dndButtonAnchor").removeClass("btn-danger");
            $("#dndButtonAnchor").addClass("btn-success");
            $("#dndButtonAnchor").html("Set DND OFF");
            setSessionDNDState(1);
            isDNDOn == true;
        }

        /**
         * <p>this turns the DND mode OFF by dialing a specific number to the PBX and changes the appearance of the DND click button in the UI</p>
         * @returns {undefined}
         * @since 2017-07-24
         * @author Sandun
         */
        function turnDndOff() {
            //            var number = "*074";
            //            document.getElementById('webphoneframe').contentWindow.registerWebphone();
            // document.getElementById('webphoneframe').contentWindow.setPhoneOnline();
            document.getElementById('webphoneframe').contentWindow.registerWebphone();

            $("#dndButtonAnchor").removeClass("btn-success");
            $("#dndButtonAnchor").addClass("btn-danger");
            $("#dndButtonAnchor").html("Set DND ON");
            setSessionDNDState(0);
            isDNDOn == false;
        }

        $(document).ready(function () {

            var value = '<?php echo $contacts ?>';
            addNewContactToList(value);


            $("#dndSwitch").change(function (event) {

                if ($("#dndSwitch").prop('checked') == true) {
                    // DND is set to ON
                    turnDndOn();
                    timeoutAgentDND();
                } else {
                    // DND is set to OFF
                    turnDndOff();
                }
            });

            $("#dndButtonAnchor").click(function (event) {
                $("#dndSwitch").click();
            });

            interval = setInterval(function () {
                if (webphoneRegistered == true) { //&& isDNDOn == "0"
                    turnDndOff();
                    clearWebphoneIntervalTimeout();
                }
            }, 1000);

        });

        /**
         * <p>This is to clear the JS interval used to turn on the DND mode when the webphone got registered with the PBX</p>
         * @returns {undefined}
         * @since 2017-07-24
         * @author Sandun
         */
        function clearWebphoneIntervalTimeout() {
            clearTimeout(interval);
        }

        function timeoutAgentDND() {
            setTimeout(function () {
                $("#dndSwitch").click();
            }, 15000);
        }

        function setSessionDNDState(state) {
            $.ajax({
                url: "<?= Url::to(['user/setdndsessionstate']) ?>",
                type: 'GET',
                data: {state: state},
                success: function (data, textStatus, jqXHR) {
                    //                alert(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //                alert(jqXHR.responseText);
                }
            });
        }

        function initiateContactCall(otherPhoneNumber) {
            // alert(otherPhoneNumber);
            otherPhoneNumber = "0" + otherPhoneNumber;
            document.getElementById('webphoneframe').contentWindow.removeConference(otherPhoneNumber);
        }

        function initiateContacttransfer(number) {
            number = "0" + number;
            document.getElementById('webphoneframe').contentWindow.transfer(number);
        }
        function initiateContactConference(otherPhoneNumber) {
            // contentWindow.addToConference
            // inviteForConferenceAJAX(otherPhoneNumber);

            // if(otherPhoneNumber.length > 6)
            otherPhoneNumber = "0" + otherPhoneNumber;
            document.getElementById('webphoneframe').contentWindow.addToConference(otherPhoneNumber);

        }
    </script>




    <script>
        // for the late alert functionality
        var late = "<?= $late ?>";

        function showYouAreLateAlert() {
            swal({
                title: 'You are late!',
                text: 'Talk to the supervisor immidiately',
                type: "error"
            });
        }


        $(document).ready(function () {
            if (onloadAgentRequests.length > 0) {
                setAgentRequestsOnload();
            }
            //            if (onloadAgentRequests.length > 0) {
            //                setOnloadTicket();
            //            }
            if (late == "1") {
                showYouAreLateAlert();
            }
        });
        function setOnloadTicket() {
            for (var x = 0; x < onloadAgentRequests.length; x++) {
                if (onloadAgentRequests[x][2] == 'pending') {
                    if (onloadAgentRequests[x][0] == 'lunch') {
                        changeRequestButtonColor('br1', 'btnRequestLunch', 'pending');
                    } else if (onloadAgentRequests[x][0] == 'meeting') {
                        changeRequestButtonColor('br5', 'btnRequestMeeting', 'pending');
                    } else if (onloadAgentRequests[x][0] == 'short') {
                        changeRequestButtonColor('br2', 'btnRequestShort', 'pending');
                    } else if (onloadAgentRequests[x][0] == 'sick') {
                        changeRequestButtonColor('br3', 'btnRequestSick', 'pending');
                    } else if (onloadAgentRequests[x][0] == 'other') {
                        changeRequestButtonColor('br4', 'btnRequestOther', 'pending');
                    }
                    //                    changeRequestButtonColor(btnId, handBtnId, action)
                } else if (onloadAgentRequests[x][2] == 'approved') {
                    setAgentTicket(onloadAgentRequests[x][0], onloadAgentRequests[x][1], onloadAgentRequests[x][2], onloadAgentRequests[x][3]);
                }

            }
            //            alert(onloadAgentRequests.length);
        }

        function setAgentRequestsOnload() {
            //                alert();
            for (var x = 0; x < onloadAgentRequests.length; x++) {

                showNewAgentRequest(onloadAgentRequests[x][1], onloadAgentRequests[x][2], onloadAgentRequests[x][0]);

            }
        }

        function showNewAgentRequest(id, agentName, request) {

            var newAgentRequestDiv = '';
            if (request == 'lunch') {

                newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green' id='" + id + "'  class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: blueviolet'>Lunch break</span> </div>";
                //                    console.log(newAgentRequestDiv);
            } else if (request == 'short') {
                newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green'  id='" + id + "' class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: orange'>Short break</span> </div>";
            } else if (request == 'sick') {
                newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><input type='number' style='width:45px;color: black;' id='T" + id + "'  maxlength='1'><a style='color: green'  id='" + id + "' class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,1)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #42b0f4'>Sick break</span> </div>";
            } else if (request == 'other') {
                newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green'  id='" + id + "'   class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #000cc'>Other break</span> </div>";
            } else if (request == 'meeting') {
                newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><input type='number' style='width:45px;color: black;' id='T" + id + "'  maxlength='1'><a style='color: green'  id='" + id + "'   class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,1)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #f44180'>Meeting / training break</span> </div>";
            }

            if ($(".agentRequests").length > 0) {

                $("#agentRequestsDisplayDiv").prepend(newAgentRequestDiv);
                incrementAgentRequestsCount();

            } else {

                $("#agentRequestsDisplayDiv").html("");
                $("#agentRequestsDisplayDiv").prepend(newAgentRequestDiv);
                incrementAgentRequestsCount();
            }
        }
    </script>

    <script>

        // This is to change the title to get the user's attention

        $("title").html("Agent caller profile"); // change the header title
        var timeoutTitleFlash;
        /**
         * <b>This function shows flashing suns with rays at both sides of the title to get user's attention.</b>
         * and empties the hidden last answered caller Id value
         * @returns {undefined}
         * @author Sandun
         * @since 2017-07-14
         */
        //    (function () {
        function showFlashingTitle() {
            var n = 0;
            var t = "Incomming call";
            $('#idHiddenCallerNumber').val() == ''; // This is to empty the last answered caller number identification field
            //        var f = function () {

            //            timeoutTitleFlash = setTimeout(f, 500); // every 500ms
            timeoutTitleFlash = setInterval(function () {
                n++;
                switch (n) {
                    case 3:
                        n = 1; // no break, so continue to next label
                    case 1:
                        document.title = '☏ ☏ ' + t + ' ☎ ☎';
                        break;
                    default:
                        document.title = '☎ ☎ ' + t + ' ☏ ☏';
                }
            }, 500);
        }

        //        f(); // start the animation
        //    })();

        /**
         * <b>This function clears the timer of the flashing title and </b>
         * 
         * @returns {undefined}
         * @author Sandun
         * @since 2017-07-14
         */
        function resetPageHeaderTitle() {
            clearTimeout(timeoutTitleFlash);
            $("title").html("Agent caller profile"); // change the header title
        }
    </script>
    <?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 
        <script>
            function addNewContactToList(data) {
                var numbers = $.parseJSON(data);
                $('#contactsDisplayDiv').empty();
                for (var index = 0; index < numbers.length; index++) {
                    var temp = [numbers[index]['name'], numbers[index]['number']];
                    contactNamesArray.push(temp);
                    var html1 = "<div class='list-group-item' id='" + numbers[index]['number'] + "'><span class='badge' style='float: initial;'>";
                    // var agentName = (agents[index][1].length > 4 ? agents[index][1] + ".." : agents[index][1]);
                    var agentName = numbers[index]['name'];
                    // var agentLoggedInTime = agents[index][5];
                    // if (agents[index][3] == 1) {
                    var html2 = "<a style='cursor:pointer;text-decoration: none;' onclick='initiateContactCall(" + numbers[index]['number'] + ")' class='glyphicon glyphicon-earphone' title='Take Call'>&nbsp</a><a style='cursor:pointer;text-decoration: none;' onclick='initiateContacttransfer(" + numbers[index]['number'] + ")' class='glyphicon glyphicon-transfer' title='Transfer Call'>&nbsp</a><a style='cursor:pointer; text-decoration: none;' onclick='initiateContactConference(" + numbers[index]['number'] + ")' class='glyphicon glyphicon-refresh' title='Conference Call'>&nbsp</a></span><br>" + agentName + "";
                    // } else if (agents[index][3] == 0) {
                    // var html2 = "<i class='fa fa-circle' aria-hidden='true' style='color:yellow'></i></span>" + agentName + "";
                    // }

                    var html3 = "</div>";

                    var contactEntry = html1 + html2 + html3;
                    $('#contactsDisplayDiv').append(contactEntry);

                }
            }
            function searchContact(event) {
        //                alert();
                if (event.keyCode == 13) {

                    var value = $('#idContactTxt').val();
                    searchContacts(value);
                }

            }


            function searchContacts(val) {

                $.ajax({
                    url: "<?= Url::to(['supevisor/searchcontact']) ?>",
                    type: 'POST',
                    data: {q: val},
                    success: function (data, textStatus, jqXHR) {
                        addNewContactToList(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Error" + jqXHR.rex);
                    }
                });
            }


        </script>
    <?php } ?>
    <script>
        $(document).ready(function () {
            $('.num').click(function () {
                var num = $(this);
                var text = $.trim(num.find('.txt').clone().children().remove().end().text());
                var telNumber = $('#telNumber');
                $(telNumber).val(telNumber.val() + text);
            });
            $("#keypad").click(function () {
                $(".phone").slideToggle("fast");
            });
        });</script>
    <script>
        //    function getCallInformation(phoneNumber) {
        ////        var id = $(this).attr('id');
        //        var url = "<? = Url::to(['agent/customerdetails']) ?>";
        //        $.ajax({
        //            type: "GET",
        //            url: url,
        //            data: {q: phoneNumber},
        //            success: function (data)
        //            {
        //                var customerData = JSON.parse(data);
        //                $('#idCustomerName').text(customerData['customer_name']);
        //                $('#idCustomerEmail').text(customerData['email']);
        //                $('#idCustomerContact').text(customerData['contact']);
        //                $('#idCustomerAddress').text(customerData['address']);
        //                $('#idCustomerDOB').text(customerData['dob']);
        //                $('#idCustomerGender').text(customerData['gender']);
        //                if (customerData['gender'] == "male") {
        //                    $('#idProfilePic').prop("src", "images/dummy_profile_male.jpg");
        //                } else {
        //                    $('#idProfilePic').prop("src", "images/dummy_profile_female.jpg");
        //                }
        //
        //                $('.profile-panel').show();
        //                if (customerData['callRecords'].length > 0) {
        //                    for (var i = 0; i < customerData['callRecords'].length; i++) {
        //                        var date = customerData['callRecords'][i]['start'];
        //                        var seperaterDate = date.split(" ");
        //                        addPreviousCallsRow(seperaterDate[0], seperaterDate[1], customerData['callRecords'][i]['src'], customerData['callRecords'][i]['dst'], customerData['callRecords'][i]['duration'], customerData['callRecords'][i]['lastapp']);
        //
        ////                        addPreviousCallsRow(date, time, number, answeredA, duration, status);
        //                    }
        //
        //                }
        //            },
        //            error: function (xhr, ajaxOptions, thrownError) {
        ////                alert(xhr.responseText);
        ////                alert(xhr.status);
        ////                alert(thrownError);
        ////                alert(xhr.responseText);
        //            }
        //        });
        ////        alert("Called me " + phoneNumber);
        //    }
    </script>
<?php } ?>
<style>
    /*// for the chat interface*/
    .mytext{
        border:0;padding:10px;background:whitesmoke;width: 100%;
    }
    .text{
        width:75%;display:flex;flex-direction:column;
    }
    .text > p:first-of-type{
        width:100%;margin-top:0;margin-bottom:auto;line-height: 13px;font-size: 12px;
    }
    .text > p:last-of-type{
        width:100%;text-align:right;color:silver;margin-bottom:-7px;margin-top:auto;
    }
    .text-l{
        float:left;padding-right:10px;
    }        
    .text-r{
        float:right;padding-left:10px;
    }
    .avatar{
        display:flex;
        justify-content:center;
        align-items:center;
        width:25%;
        float:left;
        padding-right:10px;
    }
    .macro{
        margin-top:5px;width:85%;border-radius:5px;padding:5px;display:flex;
    }
    .msj-rta{
        float:right;background:whitesmoke;
    }
    .msj{
        float:left;background:white;
    }
    .frame{
        background:#e0e0de;
        height:450px;
        overflow:hidden;
        padding:0;
        position: absolute;
        z-index: 1;
    }
    .frame > div:last-of-type{
        position:absolute;bottom:5px;width:100%;display:flex;
    }
    #chatPanelDiv > ul {
        width:100%;
        list-style-type: none;
        padding:18px;
        position:absolute;
        bottom:32px;
        display:flex;
        flex-direction: column;

    }
    .msj:before{
        width: 0;
        height: 0;
        content:"";
        top:-5px;
        left:-14px;
        position:relative;
        border-style: solid;
        border-width: 0 13px 13px 0;
        border-color: transparent #ffffff transparent transparent;            
    }
    .msj-rta:after{
        width: 0;
        height: 0;
        content:"";
        top:-5px;
        left:14px;
        position:relative;
        border-style: solid;
        border-width: 13px 13px 0 0;
        border-color: whitesmoke transparent transparent transparent;           
    }  

    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #d4d4d4;
    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: #d4d4d4;
    }
    :-ms-input-placeholder { /* IE 10+ */
        color: #d4d4d4;
    }
    :-moz-placeholder { /* Firefox 18- */
        color: #d4d4d4;
    }   

    /* CSS used here will be applied after bootstrap.css */
    .badge-notify{
        background:red;
        position:relative;
        top: -10px;
        left: -15px;
    } 
    .btnRequestLunchDuration{
        display:none;
    }
    .btnRequestShortDuration{
        display:none;
    }
    .btnRequestSickDuration{
        display:none;
    }
    .btnRequestOtherDuration{
        display:none;
    }
    .btnRequestMeetingDuration{
        display:none;
    }


</style>

<?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 

    <script>
        // for chatting cuntionality
        //            var me = {};
        //            me.avatar = "https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48";
        //            var you = {};
        //            you.avatar = "https://a11.t26.net/taringa/avatares/9/1/2/F/7/8/Demon_King1/48x48_5C5.jpg";
        //            function formatAMPM(date) {
        //            var hours = date.getHours();
        //                    var minutes = date.getMinutes();
        //                    var ampm = hours >= 12 ? 'PM' : 'AM';
        //                    hours = hours % 12;
        //                    hours = hours ? hours : 12; // the hour '0' should be '12'
        //                    minutes = minutes < 10 ? '0' + minutes : minutes;
        //                    var strTime = hours + ':' + minutes + ' ' + ampm;
        //                    return strTime;
        //            }
        //
        ////-- No use time. It is a javaScript effect.
        //    function insertChat(who, text, time = 0) {
        //    var control = "";
        //            var date = formatAMPM(new Date());
        //            if (who == "me") {
        //
        //    control = '<li style="width:100%">' +
        //            '<div class="msj macro">' +
        //            '<div class="avatar"><img class="img-circle" style="width:100%;" src="' + me.avatar + '" /></div>' +
        //            '<div class="text text-l">' +
        //            '<p>' + text + '</p>' +
        //            '<p><small>' + date + '</small></p>' +
        //            '</div>' +
        //            '</div>' +
        //            '</li>';
        //    } else {
        //    control = '<li style="width:100%;">' +
        //            '<div class="msj-rta macro">' +
        //            '<div class="text text-r">' +
        //            '<p>' + text + '</p>' +
        //            '<p><small>' + date + '</small></p>' +
        //            '</div>' +
        //            '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="' + you.avatar + '" /></div>' +
        //            '</li>';
        //    }
        //    setTimeout(
        //            function () {
        //            $("#chatPanelDiv ul").append(control);
        //            }, time);
        //    }
        //
        //    function resetChat() {
        //    $("#chatPanelDiv ul").empty();
        //    }
        //
        //    $(".mytext").on("keyup", function (e) {
        //    if (e.which == 13) {
        //    var text = $(this).val();
        //            if (text !== "") {
        //    insertChat("me", text);
        //            $(this).val('');
        //    }
        //    }
        //    });
        ////-- NOTE: No use time on insertChat.
        //
        //            function demoChat() {
        //            //-- Clear Chat
        //            resetChat();
        ////-- Print Messages
        //                    insertChat("me", "Hello Tom...", 0);
        //                    insertChat("you", "Hi, Pablo", 1500);
        //                    insertChat("me", "What would you like to talk about today?", 3500);
        //                    insertChat("you", "Tell me a joke", 7000);
        //                    insertChat("me", "Spaceman: Computer! Computer! Do we bring battery?!", 9500);
        //                    insertChat("you", "LOL", 12000);
        //            }
        //
        //    $(document).ready(function () {
        //    $("#chatOpenCloseAnchor").click(function () {
        //    $("#chatPanelDiv").slideToggle("slow");
        //            if ($("#chatOpenCloseAnchor").text() == "Open Chat") {
        //    // the cat window is opened
        //    $("#chatOpenCloseAnchor").removeClass("btn-success");
        //            $("#chatOpenCloseAnchor").addClass("btn-danger");
        //            $("#chatOpenCloseAnchor").text("Hide Chat");
        //            $("#chatOpenCloseAnchor").css({"z-index": "2", "position": "absolute", "width": "90%"});
        //            demoChat();
        //    } else {
        //    // the chat window is closed
        //    $("#chatOpenCloseAnchor").removeClass("btn-danger");
        //            $("#chatOpenCloseAnchor").addClass("btn-success");
        //            $("#chatOpenCloseAnchor").text("Open Chat");
        //            $("#chatOpenCloseAnchor").css({"z-index": "", "position": "", "width": ""});
        //    }
        //
        //    });
        //            $('#breakRequestOpenCloseAnchor').click(function () {
        //    $("#breakRequestPanel").slideToggle("slow");
        //    });
        //    });</script>

    <script>//$("title").html("Agent Overview");</script>
    <script>
        // for webphone functionalities

        var serverAddress = "<?= $webphoneParams['serverAddress'] ?>";
        var username = "<?= $webphoneParams['username'] ?>";
        var password = "<?= $webphoneParams['password'] ?>";

    </script>

<?php } ?>

<div style="font-family: 'Lato' , sans-serif;font-weight: bold">
    <div class="row form-group" style="font-family: 'Lato' , sans-serif;font-weight: bolder">
        <?php if (!isset($_GET['agentmode'])) { ?>
            <div class="col-xs-9" style="padding-right: 0px;">
            <?php } else { ?>
                <div class="col-xs-9" style="padding-right: 0px; text-align: center">
                <?php } ?>
                <a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-dashboard"></span> <?= Yii::$app->session->get('user_name') ?> : <?= Yii::$app->session->get('voip') ?></a>         
                <?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 
                    <a href="<?= Url::to(['reports/performanceoverview']) ?>" class="btn btn-danger"><span class="glyphicon glyphicon-stats"></span> Performance Overview</a>
                    <!--<a href="#" class="btn btn-primary btn-warning" id="helpRequestAnchor"><span class="glyphicon glyphicon-hand-up"></span> Help</a>-->
                <?php } ?>
                <!--            <button class="btn btn-info">
                                <span class="glyphicon glyphicon-comment"></span>
                            </button>
                            <span class="badge badge-notify">5</span>-->
                <input style="width: 250px;font-size: 110%;height: 40px;" class="input-lg " id="customerPhoneNumberSearchInput" name="customerPhoneNumberSearchInput" placeholder="Phone number or Vehicle number" onkeypress="pressEnterSearch(event)">
            </div>
            <div class="col-xs-3" style="text-align: left">
            <!--<img id="loadingGif" style="height: 40px;padding-bottom: 12px; display: none" src="hnb_images/loading.gif">-->   
                <i id="loadingCallDataIndicator" style="display: none;" class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                <label id="callDataStatusLabel" class="alert alert-info" style="margin-bottom: 0%; display: none">No customer data</label>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-2">
                <?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 
                    <!--Agent Contact List Start--> 
                    <div style="border: 1px solid #10297d;border-radius: 5px">
                        <div class="row">
                            <div class="col-xs-12" style="height: 225px;">
                                <a href="#" class="list-group-item active" style="height:35px;">
                                    <div class="row">
                                        <div class="col-xs-4 headertitles" style="font-size: 150%; font-style: normal">Contacts</div>
                                        <div class="col-xs-8" style="text-align: right"></div>
                                    </div>
                                </a>                            

                                <div id="contactsDisplayDiv" class="list-group" style="height: 155px;overflow-y: scroll;font-size: 90%; background-color: white; margin-bottom:6px">
                                    <div class="list-group-item" style="border: #fff" id="noContactsDiv">
                                        <span style="font-weight: bold">No Contacts</span>
                                    </div>

                                </div>
                                <input type="text" id="idContactTxt" style="width:130px; margin:2px" placeholder="Contact Name ..." onkeypress="searchContact(event)">
                            </div>
                        </div>
                    </div>
                    <!--Agent Contact List End-->
                    <br>


                    <div class="panel panel-default" style="border: 1px solid #10297d">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a>Agents</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="" style="height: 280px;overflow-y: scroll">
                            <ul id="agentListGroup" class="list-group" style="">

                            </ul>
                        </div>                        
                    </div>
                    <br>

                <?php } ?>
                <br>
                <input type="hidden" id="idHiddenCallerNumber" name="idHiddenCallerNumber" value="">
            </div>
            <div class="col-xs-7">                
                <div class="row">
                    <!-- Customer general information section -->
                    <div class="col-xs-6">
                        <div class="panel profile-panel" style="border: 1px solid #10297d;border-radius: 5px">
                            <div class="panel-heading" style="height: 40px;background-color: #10297d;color: #fff">
                                <div class="col-md-9">
                                    <h3 id="idCustomerName" class="panel-title customerInfo"></h3>                            
                                </div>
                                <div class="col-md-3" align="right">
                                    <!--<span class="sr-only">Loading...</span>-->
                                    <!--<img id="ongoingCallImg" style="height: 40px;padding-bottom: 15px" src="hnb_images/ongoing-call.png">-->   
                                    <i id="ongoingCallImg" class="fa fa-user-o fa-2x" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class=" col-md-12 col-lg-12 " id="customerGeneralInformationDiv" style="display: block; overflow-y: scroll; height: 250px">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Policy Number</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerPolicyNo" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>NIC</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerNIC" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Date of Birth</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerDOB" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Home Address</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerAddress" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Contact Number</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerContact" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Agent / Broker</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerAgentBroker" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Next Due Date</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerNextDueDate" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Premium Mode</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerPremiumMode" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Term</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerTerm" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Table</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idCustomerTable" class="customerInfo"></span>
                                            </div>
                                        </div>
                                        <hr>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        #customerGeneralInformationDiv > hr{
                            margin-top: 3%;
                            margin-bottom: 3%;
                        } 

                        #policyInformationDiv > hr{
                            margin-top: 3%;
                            margin-bottom: 3%;
                        } 
                    </style>
                    <!-- End of Customer general information section -->
                    <!-- policies section -->
                    <div class="col-xs-6">
                        <div class="panel profile-panel" style="border: 1px solid #10297d;border-radius: 5px">
                            <div class="panel-heading" style="height: 40px; background-color: pink;color: #fff">
                                <div class="col-md-9">
                                    <h3 id="policyTitle" class="panel-title">Life Policy Information</h3>                            
                                </div>
                                <div class="col-md-3" align="right">
                                    <!--<i id="loadingCallDataIndicator" style="display: none" class="fa fa-refresh fa-spin fa-lg fa-fw"></i>-->
                                    <!--<span class="sr-only">Loading...</span>-->
                                    <!--<img id="ongoingCallImg" style="height: 40px;padding-bottom: 15px" src="hnb_images/ongoing-call.png">-->     
                                    <i id="policySectionGlymphycon" class="fa fa-file fa-2x" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <!--                        <div class="col-md-3 col-lg-3 " align="center">
                                                                <img alt="User Pic" id="idProfilePic" src="<? = $profileImageSrc ?>" class="img-circle img-responsive"> 
                                                            </div>-->

                                    <!-- Policies list section -->
                                    <div id="policyInformationDiv" class=" col-xs-12 col-md-12 col-lg-12" style="display: block; overflow-y: scroll; height: 250px">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Channel Name</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyChannelName" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Assurance Code</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyAssuranceCode" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Sum Insured</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicySumAssured" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Branch</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyBranch" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Paid Total</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyPaidTotal" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Count OD Inst. Paid</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyCountOfInstPaid" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Commencement Date</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyCommencementDate" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Maturity Date</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyMaturityDate" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Application Date</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyApplicationDate" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Vested Bonus</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyVestedBonus" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Paid up</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyPaidUp" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>PID</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyPID" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <span>Premium</span>
                                            </div>
                                            <div class="col-xs-6">
                                                <span id="idPolicyPremium" class="policyInfo"></span>
                                            </div>
                                        </div>
                                        <hr>

                                    </div>
                                    <!-- End of Policies list section -->

                                    <!-- Policies list section -->
                                    <div id="policiesListDiv" class=" col-xs-12 col-md-12 col-lg-12" style="display: none; overflow-y: scroll; height: 250px">
                                        <!--                                        <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <a class="btn btn-block btn-success">GAC - 2233</a>
                                                                                    </div>
                                                                                </div>-->
                                    </div>
                                    <!-- End of Policies list section -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of policies section -->
                </div>
                <div class="row" style="margin-top: 10px">
                    <div class="col-xs-12" style="text-align: left;" >
                        <strong style="font-size: 110%; ">Previous calls</strong>
                        <table class="table table-fixed table-striped call-history-table" id="callerHistoryTable" style="border: 1px solid #10297d;border-radius: 5px">
                            <thead class="call-history-thead">
                                <tr>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Number</th>                                
                                    <th>Agent Answered</th>
                                    <th>Call Duration</th>                                
                                    <th>Call Status</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody class="call-history-tbody" style="text-align: center">

                            </tbody>
                        </table>                    
                    </div>
                </div>

                <style>
                    .call-history-table {
                        width: 100%;
                    }

                    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
                        display: block;
                    }

                    .call-history-thead th {
                        height: 50px;
                    }

                    .call-history-tbody {
                        overflow-y: auto;
                        height: 120px;
                        font-size: 90%;
                    }

                    .call-history-tbody td, .call-history-thead th {
                        float: left;
                        width: 13.66%;
                    }

                    .call-history-thead tr:after, call-history-tbody tr:after {
                        clear: both;
                        content: ' ';
                        display: block;
                        visibility: hidden;
                    }    

                </style>
                <div class="row">
                    <div class="col-xs-12" style="text-align: right">
                        <textarea id="callEventDescription" name="callEventDescription" class="form-control" style="height: 60px" placeholder="Feed call information"></textarea>
                        <!--<button id="descSaveAnchor" class="btn btn-md btn-default" style="margin-top: 2px; color: #faa61a; background-color: #10297d">Save</button>-->
                        <a id="descSaveAnchor" class="btn btn-md btn-default" style="margin-top: 2px; color: #faa61a; background-color: #10297d"><b>Save</b></a>
                        <!--<a id="descClearAnchor" class="btn btn-md btn-primary" style="margin-top: 2px; color: #faa61a; background-color: #10297d"><b>Close Job</b></a>-->
                    </div>


                </div>

            </div>
            <div class="col-xs-3"   style="padding-left: 0%;padding-right: 0%">
                <?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="phone">
                                <a class="btn btn-block btn-primary" style="margin-top: 0%" onclick="document.getElementById('webphoneframe').src = 'softphone/softphone.html'">Refresh Phone</a>
                                <iframe frameborder="0" width="291" height="500" src="softphone/softphone.html" name="wphone" id="webphoneframe"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php
                            if (Yii::$app->session->get('dnd') == TRUE) {
                                // DND is ON
                                ?>
                                <input style="display: none" type="checkbox" name="dndSwitch" data-toggle="toggle" id="dndSwitch" checked="">
                                <a id="dndButtonAnchor" class="btn btn-block btn-success">Set DND OFF</a>
                                <?php
                            } else {
                                // DND is OFF
                                ?>
                                <input style="display: none" type="checkbox" name="dndSwitch" data-toggle="toggle" id="dndSwitch">
                                <a id="dndButtonAnchor" class="btn btn-block btn-danger">Set DND ON</a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <br> 

                <?php } ?>
            </div>


        </div>
        <br>
        <div class="row">
            <div class="col-xs-6">
                <!-- Help requests section -->
                <div style="border: 1px solid #10297d;border-radius: 5px">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" class="list-group-item active">
                                <div class="row">
                                    <div class="col-xs-9 headertitles">Help Requests</div>
                                    <div class="col-xs-3 col-xs-pull-1" style="text-align: left;"><span id="helpRequestsCounter" class="btn btn-xs btn-warning" style="margin-top: 0%">0</span></div>
                                </div>
                            </a>                            
                            <div id="helpRequestsDisplayDiv" class="list-group" style="height: 155px;overflow-y: scroll;font-size: 90%; background-color: white">
                                <div class="list-group-item" style="border: #fff" id="noHelpRequestsDiv">
                                    <span style="font-weight: bold">No help requests</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- End of Help requests section -->
            </div>
            <div class="col-xs-6">
                <!-- Agent requests section-->
                <div style="border: 1px solid #10297d;border-radius: 5px">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" class="list-group-item active">
                                <div class="row">
                                    <div class="col-xs-9 headertitles">Agent Requests</div>
                                    <div class="col-xs-3 col-xs-pull-1" style="text-align: left;"><span id="agentRequestsCounter" class="btn btn-xs btn-warning" style="margin-top: 0%">0</span></div>
                                </div>
                            </a>    
                            <div id="agentRequestsDisplayDiv" class="list-group" style="height: 155px;overflow-y: scroll;font-size: 90%; background-color: white">
                                <div class='list-group-item' style='border: #fff' id='noAgentRequestsDiv'><span style='font-weight: bold'>No agent break requests</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Agent requests section -->
            </div>
        </div>
    </div>
    <script>

        $('#descSaveAnchor').click(function () {
            if ($("#callEventDescription").val() == "") {
                // comment is empty
//                    alert("Please enter a comment");
                swal({
                    title: 'Please enter a comment!',
                    type: "warning"
                });
            } else {
                saveCallComment();
            }
        });
//        $('#descClearAnchor').click(function () {
//            $('#callEventDescription').val('');
//            $('#idHiddenCallerNumber').val('');
//
//        });

    </script>
    <script>

        // This function will add new row in callerHistoryTable
        function addPreviousCallsRow(date, time, number, answeredA, duration, status, comment) {
            var tableRef = document.getElementById('callerHistoryTable').getElementsByTagName('tbody')[0];

            // Insert a row in the table at row index 0
            var newRow = tableRef.insertRow(tableRef.rows.length);
            newRow.setAttribute("data-toggle", "tooltip");
            newRow.setAttribute("title", comment);

            if (status == "Missed") {
                $(newRow).css('color', 'red');
            }

//            alert("length = "+comment);
            if (comment != null) {
                if (comment.length > 10) {
                    comment = comment.substring(0, 9); // Extracts the first 10 characters of the comment
                    comment += "..";
                }
            }

            // Insert a cell in the row at index 0
            var newCell1 = newRow.insertCell(0);
            var newCell2 = newRow.insertCell(1);
            var newCell3 = newRow.insertCell(2);
            var newCell4 = newRow.insertCell(3);
            var newCell5 = newRow.insertCell(4);
            var newCell6 = newRow.insertCell(5);
            var newCell7 = newRow.insertCell(6);


            // Append a text node to the cell
            var newText1 = document.createTextNode(date);
            var newText2 = document.createTextNode(time);
            var newText3 = document.createTextNode(number);
            var newText4 = document.createTextNode(answeredA);
            var newText5 = document.createTextNode(duration + " sec");
            var newText6 = document.createTextNode(status);
            var newText7 = document.createTextNode(comment);

            newCell1.appendChild(newText1);
            newCell2.appendChild(newText2);
            newCell3.appendChild(newText3);
            newCell4.appendChild(newText4);
            newCell5.appendChild(newText5);
            newCell6.appendChild(newText6);
            newCell7.appendChild(newText7);
        }
        function answerCall(number) {
//        alert("answered "+number);

            if (number.length > 6) {
//                number = '771980774';
                $.ajax({
                    type: "POST",
                    url: "<?= Url::to(['agent/savecallrecord']) ?>",
                    data: {callerNumber: number},
                    success: function (data) {
                        if (data != 0) {
//
//
//                        var jsonDecodedResponse = JSON.parse(data);
                            $('#idHiddenCallerNumber').val(data);
//                        $('#idCustomerName').text(jsonDecodedResponse['customer_insuered_name']);
//                        $('#idCustomerDOB').text(jsonDecodedResponse['customer_dob']);
//                        $('#idCustomerGender').text(jsonDecodedResponse['customer_gender']);
//                        $('#idCustomerAddress').text(jsonDecodedResponse['customer_address']);
//                        $('#idCustomerEmail').text(jsonDecodedResponse['customer_email']);
//                        $('#idCustomerContact').text(jsonDecodedResponse['customer_contact']);
//
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //                        alert(jqXHR.responseText);
                        swal({
                            title: 'Error!',
                            text: 'Success Something happened. Call record Did not save!',
                            type: "danger",
                            timer: 2000
                        });
                    }
                });
            }
        }

        function clearCallerInformationFromCall() {
            // called by an incoming call
            if ($('#idHiddenCallerNumber').val() == '') {
                // This call was not answered
//                $('#idHiddenCallerNumber').val("");
//                $(".customerInfo").text("");
//                $(".policyInfo").text("");
//                $("#policiesListDiv").empty();
//
//                $("#callerHistoryTable > tbody").empty();
                clearCallerInformationFromSearch();
            }

        }

        function clearCallerInformationFromSearch() {
            $('#idHiddenCallerNumber').val("");
            $(".customerInfo").text("");
            $(".policyInfo").text("");
            $("#policiesListDiv").empty();
            $("#callerHistoryTable > tbody").empty();

        }

        function populatePreviousCallsTable(callRecordsObject) {
            console.log("0th : called populatePreviousCallsTable");
            for (var x = 0; x < callRecordsObject.length; x++) {
//            callRecordsObject[x][''];
                console.log("0th : writing " + x + " record");
                var date = callRecordsObject[x]['start'];
                var seperaterDate = date.split(" ");
//            console.log("keys = " + Object.keys(callRecordsObject[x]));
//            alert("comment = " + callRecordsObject[x]['comment']);
                addPreviousCallsRow(seperaterDate[0], seperaterDate[1], callRecordsObject[x]['src'], callRecordsObject[x]['dst'], callRecordsObject[x]['duration'], callRecordsObject[x]['status'], callRecordsObject[x]['comment']);
            }



        }

        var vehiclePoliciesArray = new Array(); // Global vehicle policies array

        /**
        * 

         * @param {type} number
         * @returns {undefined}       
         * @author Sandun
         * @modified Sandun 2017-09-27
         * @description added show previous call records part to the no customer data section to show them even if the api data is not available  
         * @modified Sandun 2017-10-17
         * @description Moved the customer and vehicle information print code to 2 functions and invokes them here.
         * If mutiple policies received, while the vehicle number button print in the view, all the policy and customer data associated to it pushes to vehiclePoliciesArray as objects
         * */
        function showCustomerData(number) {
//        alert("Ringing "+number);
//            number = '771980774'; // for testing only
//            $("#ongoingCallImg").css("display", "none");
            $("#loadingCallDataIndicator").css("display", "block");
            $("#callDataStatusLabel").css("display", "none");
            clearCallerInformationFromSearch();
            $.ajax({
                url: "<?= Url::to(['agent/getcustomerinformation']) ?>",
                type: 'POST',
                data: {number: number},
                success: function (data, textStatus, jqXHR) {
//                    alert(data);
//                    $("#ongoingCallImg").css("display", "block");
                    $("#loadingCallDataIndicator").css("display", "none");
                    var jsonDecodedResponse = JSON.parse(data);

                    if (jsonDecodedResponse['customer_policies'] != 0) {
                        // has customer data
//                        $('#idHiddenCallerNumber').val(jsonDecodedResponse['record_id']);
//                        $('#idCustomerName').text(jsonDecodedResponse['customer_insuered_name']);
//                        $('#idCustomerPolicyNo').text(jsonDecodedResponse['customer_policyNumber']);
//                        $('#idCustomerNIC').text(jsonDecodedResponse['customer_NIC']);
//                        $('#idCustomerAddress').text(jsonDecodedResponse['customer_address']);
//                        $('#idCustomerContact').text(jsonDecodedResponse['customer_contact']);
//                        $('#idCustomerEffectiveDate').text(jsonDecodedResponse['customer_effectiveDate']);
//                        $('#idCustomerExpiryDate').text(jsonDecodedResponse['customer_expiryDate']);
//                        $('#idCustomerPolicyType').text(jsonDecodedResponse['customer_policyType']);
//                        $('#idCustomerStatus').text(jsonDecodedResponse['customer_policyStatus']);
//                        $('#idCustomerVIP').text(jsonDecodedResponse['customer_isVIP']);
                        showCustomerInformation(jsonDecodedResponse);



                        var callRecordsObject = jsonDecodedResponse['callRecords'];
                        if (callRecordsObject.length > 0) {
                            populatePreviousCallsTable(callRecordsObject);
                        }


                        if (jsonDecodedResponse['customer_policies'] == 1) {
                            // has only one policy
//                            alert("1 policy");
//                            $("#policyTitle").text("Vehicle Information");
//                            $("#policyInformationDiv").css("display", "block"); // shows single policy information section
//                            $("#policiesListDiv").css("display", "none"); // hides policy list section
//
//                            $('#idPolicyVehicleNumber').text(jsonDecodedResponse['policy_vehicleNumber']);
//                            $('#idPolicySumInsured').text(jsonDecodedResponse['policy_sumInsured']);
//                            $('#idPolicyEngineNumber').text(jsonDecodedResponse['policy_engineNumber']);
//                            $('#idPolicyChasisNumber').text(jsonDecodedResponse['policy_chasisNumber']);
//                            $('#idPolicyVehicleCategory').text(jsonDecodedResponse['policy_vehicleCategory']);
//                            $('#idPolicyCubicCapacity').text(jsonDecodedResponse['policy_cubicCapacity']);
//                            $('#idPolicyYearOfMake').text(jsonDecodedResponse['policy_yearOfMake']);
//                            $('#idPolicyMake').text(jsonDecodedResponse['policy_make']);
//                            $('#idPolicyModel').text(jsonDecodedResponse['policy_model']);
//                            $('#idPolicyVehicleUsage').text(jsonDecodedResponse['policy_vehicleUsage']);

                            showVehicleInformation(jsonDecodedResponse);
                        } else {
                            // has more than 1 policies
//                            alert("more than 1 policies");
                            $("#policyTitle").text("Select Policy");
                            $("#policyInformationDiv").css("display", "none"); // hides single policy information section
                            $("#policiesListDiv").css("display", "block"); // shows policy list section
                            

                            var vehicleNumbersObject = jsonDecodedResponse['lifePolicyNumbers'];
                            vehiclePoliciesArray = new Array();
                            for (var x = 0; x < vehicleNumbersObject.length; x++) {
                                var newRow = $("<div></div>").addClass("row");//document.createElement("div");
                                var newColumn = $("<div></div>").addClass("col-xs-12");
                                var newAnchorButton = $("<a></a>").addClass("btn btn-block btn-primary vehicleNumber");
                                newAnchorButton.text(vehicleNumbersObject[x]['customer_policyNumber']);
                                newAnchorButton.prop("id", x);

                                newColumn.prepend(newAnchorButton);
                                newRow.prepend(newColumn);
                                $("#policiesListDiv").prepend(newRow);
                                vehiclePoliciesArray.push(vehicleNumbersObject);
                            }
                            
                        }


                    } else {
                        // no customer data
                        $("#loadingCallDataIndicator").css("display", "none");
                        $("#callDataStatusLabel").css("display", "block");
                        
                        var callRecordsObject = jsonDecodedResponse['callRecords'];
                        if (callRecordsObject.length > 0) {
                            populatePreviousCallsTable(callRecordsObject);
                        }
                        
                        setTimeout(function () {
                            $("#callDataStatusLabel").css("display", "none");
                        }, 3000);
                    }

//                jsonDecodedResponse['callRecords'][0]['']
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    $("#ongoingCallImg").css("display", "block");
                    $("#loadingCallDataIndicator").css("display", "none");
                    $("#callDataStatusLabel").css("display", "block");
                    setTimeout(function () {
                        $("#callDataStatusLabel").css("display", "none");
                    }, 3000);
                }

            });
        }

        /**
        * 

         * @type type         
         * @modified Sandun 2017-10-17
         * @description instead of running the vehicle number back to the api, gets the ID of the clicked button,
         * gets its associated policy object from the vehiclePoliciesArray and prints the data in the customer and vehicle information section*/
        $(document).ready(function () {
            $("#policiesListDiv").on("click", ".vehicleNumber", function (event) {
                var vehicleNumber = event.target.text;
                var elementId = event.target.id;
//                alert(vehiclePoliciesArray[elementId]);
                showCustomerInformation(vehiclePoliciesArray[0][elementId]);
                showVehicleInformation(vehiclePoliciesArray[0][elementId]);
               // showCustomerData(vehicleNumber);
            });
        });
        
                /**
        * 
         * <b>Shows vehicle information returned from the customer api, clears the vehiclePoliciesArray at the end</b>
         * @param {type} vehiclePolicyObject
         * @returns {undefined}    
         * @author Sandun
         * @since 2017-10-17     
         *      */
        function showVehicleInformation(vehiclePolicyObject){
            $("#policyTitle").text("Life Policy Information");
            $("#policyInformationDiv").css("display", "block"); // shows single policy information section
            $("#policiesListDiv").css("display", "none"); // hides policy list section
            //
            $('#idPolicyChannelName').text(vehiclePolicyObject['customer_ChannelName']);
            $('#idPolicyAssuranceCode').text(vehiclePolicyObject['customer_AssuranceCode']);
            $('#idPolicySumAssured').text(vehiclePolicyObject['customer_SumAssured']);
            $('#idPolicyBranch').text(vehiclePolicyObject['customer_Branch']);
            $('#idPolicyPaidTotal').text(vehiclePolicyObject['customer_PaidTotal']);
            $('#idPolicyCountOfInstPaid').text(vehiclePolicyObject['customer_CountODInstPaid']);
            $('#idPolicyCommencementDate').text(vehiclePolicyObject['customer_CommencementDate']);
            $('#idPolicyMaturityDate').text(vehiclePolicyObject['customer_MaturityDate']);
            $('#idPolicyApplicationDate').text(vehiclePolicyObject['customer_ApplicationDate']);
            $('#idPolicyVestedBonus').text(vehiclePolicyObject['customer_VestedBonus']);
            $('#idPolicyPaidUp').text(vehiclePolicyObject['customer_PaidUP']);
            $('#idPolicyPID').text(vehiclePolicyObject['customer_PID']);
            $('#idPolicyPremium').text(vehiclePolicyObject['customer_Premium']);
            vehiclePoliciesArray = new Array();
        }
        
        /**
        * 
        * <b>Shows customer information returned from the customer api</b>
        * 
         * @param {type} policyObject
         * @returns {undefined}         
         * @author Sandun
         * @since 2017-10-17
         * */
        function showCustomerInformation(policyObject){
//            $('#idHiddenCallerNumber').val(policyObject['record_id']);
            $('#idCustomerName').text(policyObject['customer_insuered_name']);
            $('#idCustomerPolicyNo').text(policyObject['customer_policyNumber']);
            $('#idCustomerNIC').text(policyObject['customer_NIC']);
            $('#idCustomerDOB').text(policyObject['customer_DOB']);
            $('#idCustomerContact').text(policyObject['customer_contact']);
            $('#idCustomerAddress').text(policyObject['customer_address']);
            $('#idCustomerAgentBroker').text(policyObject['customer_AgentBroker']);
            $('#idCustomerNextDueDate').text(policyObject['customer_NextDueDate']);
            $('#idCustomerPremiumMode').text(policyObject['customer_premiumMode']);
            $('#idCustomerTerm').text(policyObject['customer_Term']);    
            $('#idCustomerTable').text(policyObject['customer_Table']);    
        }


        var hungupCallInterval;
        var hungupCallLoopIterations = 0;

        function hungUpCall() {

            // hungupCallInterval = setInterval(function () {
            var id = $('#idHiddenCallerNumber').val();
            if (id != 0 && id != '') {
                $.ajax({
                    type: "POST",
                    url: "<?= Url::to(['agent/hungupcallrecord']) ?>",
                    data: {callerId: id},
                    success: function (data) {
//                            alert(data);
                        if (data == 1) {
                            clearHungUpCallTimeout();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                    alert(jqXHR.responseText);
                    }
                });
            }
            // hungUpCallLoopIterationsCheck();
            // }, 3000);

        }

        /**
         * <b></b>
         * <p>This function clears the interval invoked from the hung up call event</p>
         */
        function clearHungUpCallTimeout() {
            clearInterval(hungupCallInterval);
        }

        /**
         * <b></b>
         * <p>This function checks the iterations the hung up call loop went, if it reached 20, the lopp will be cleared, if not the iterator count will not be incrementing</p>
         * @returns {undefined}
         */
        function hungUpCallLoopIterationsCheck() {
            if (hungupCallLoopIterations == 20) {
                hungupCallLoopIterations = 0;
                clearHungUpCallTimeout();
            } else {
                hungupCallLoopIterations++;
            }
        }

        var callerNumberForComment = null; // keeps the caller number for comment

        /**
         * 
         * @returns {undefined}
         * @modified Sandun 2017-09-27
         * @description validations to check if the caller number is null or the number is greater than 6 characters, if true sends the caller number and comment through a POST request to the server via AJAX
         * If the return is successfull, a callnumber and comment will be cleared, if failed an eeror message will pop up
         */
        function saveCallComment() {
            if (callerNumberForComment != null && callerNumberForComment.length > 6) {
                var comment = $('#callEventDescription').val();
                $.ajax({
                    type: "POST",
                    url: "<?= Url::to(['agent/savecallrecordcomment']) ?>",
                    data: {callerNumber: callerNumberForComment, commentText: comment},
                    success: function (data) {
//                        alert(data);
                        if (data == 1) {
                            callerNumberForComment = null;
                            $('#callEventDescription').val("");
                            swal({
                                title: 'Saved!',
                                text: 'Record data updated',
                                type: "success"
                            });
                        } else {
                            swal({
                                title: 'Comment didnt save correctly, Please click on save again!',
                                type: "warning"
                                        //                    timer: 2000
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(jqXHR.responseText);
                        swal({
                            title: 'Error!',
                            text: 'Some error occured!!',
                            type: "danger",
                            timer: 1000
                        });
                    }
                });
            } else {
                swal({
                    title: 'Caller number is not correct!',
                    type: "warning"
//                    timer: 2000
                });
            }

//            var id = $('#idHiddenCallerNumber').val();
//            if (id != '' && id != 0) {
//
//            }
        }


    </script>

    <?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 
        <script>
            // online users section
            var number = 0;
            //    if (typeof (EventSource) !== "undefined") {
            //        var source = new EventSource("<? = Url::to(['events/liveagents']) ?>");
            //        source.onmessage = function (event) {
            //            console.log("timestamp " + Date.now() + " : " + number);
            //            number++;
            //
            //
            //        };
            //    } else {
            //        document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
            //    }

            function showOnlineAgents(data) {
                var events_data_array = $.parseJSON(data);
                var agents = events_data_array[0];
                var agents_notif = events_data_array[2];
                //            alert(agents);
                //    console.log("agent notification "+agents_notif+" : "+number);
                //    number++;
                if (data !== 'no') {
                    $('#agentListGroup').empty();
                    for (var index = 0; index < agents.length; index++) {
                        var html1 = "<div class='list-group-item'><span class='badge'>";
                        var agentName = (agents[index][1].length > 4 ? agents[index][1] + ".." : agents[index][1]);
                        var agentLoggedInTime = agents[index][5];
                        var isagentBusy = agents[index][6];
                        if (agents[index][3] == 1 && isagentBusy == 0) {
                            // agent is online and available
                            var html2 = "<a style='cursor:pointer;text-decoration: none;' onclick='initiatetransfer(" + agents[index][4] + ")' class='glyphicon glyphicon-transfer' title='Transfer Call'>&nbsp</a><a style='cursor:pointer;text-decoration: none;' onclick='initiateConference(" + agents[index][4] + ")' class='glyphicon glyphicon-refresh' title='Conference Call'>&nbsp</a><i class='fa fa-circle' aria-hidden='true' style='color:green'></i></span>" + agentName + "<br><b class='onlineTime'>" + agentLoggedInTime + "</b>";
                        } else if (agents[index][3] == 1 && isagentBusy == 1) {
                            // agent is online but busy
                            var html2 = "<i class='fa fa-circle' aria-hidden='true' style='color:red'></i></span>" + agentName + "<br><b class='onlineTime'>" + agentLoggedInTime + "</b>";
                        } else if (agents[index][3] == 0) {
                            // agent is not online
                            var html2 = "<i class='fa fa-circle' aria-hidden='true' style='color:yellow'></i></span>" + agentName + "";
                        }

                        var html3 = "</div>";

                        var agent = html1 + html2 + html3;
                        $('#agentListGroup').append(agent);
                    }

                    if (agents_notif[0] != null) {
                        if (agents_notif[1] == 1) {
                            notif({
                                msg: "Your call is whispered!",
                                type: "info",
                                position: "center",
                                height: 30,
                                width: 300,
                                timeout: 10000
                            });
                        }
                        if (agents_notif[1] == 2) {
                            notif({
                                msg: "Admin Joined!",
                                type: "warning",
                                position: "center",
                                height: 30,
                                width: 300,
                                timeout: 10000
                            });
                        }
                        if (agents_notif[1] == 3) {
                            // an agent asked you to join to his call
                            //                    console.log("Detected a conference call");
                            //                    alert($("#"+agents_notif[3]).html());
                            swal({
                                title: 'Conference call invitaion',
                                text: agents_notif[6] + " Asks you to join call!",
                                type: 'info',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: 'grey',
                                confirmButtonText: 'Join'
                            }).then(function () {
                                // code to join the call
                                //                          console.log("Accepted to join the conference call");
                                initiateConference(agents_notif[3]);
                            });

                        }
                    }
                }
            }

            function getOnlineUsersAJAX() {

                $.ajax({
                    url: "<?= Url::to(['events/liveagents']) ?>",
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        //                console.log("online agents success timestamp " + Date.now() + " : " + number);
                        //                number++;
                        showOnlineAgents(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //                console.log("online agents fail timestamp " + Date.now() + " : " + number);
                        //                number++;
                    }
                });
            }

        </script>
    <?php } ?>
    <script>
        // to fetch customer data by search
        function pressEnterSearch(event) {
            if (event.keyCode == 13) {
//                $("#loadingGif").css("display", "inline");

                //            alert(event.target.value);
                //            triggerEvent(event);
                //            alert(event.values());
//                searchCustomerRecord(event.target.value);
                showCustomerData(event.target.value);
            }
        }
        function searchCustomerRecord(val) {
            //        alert();
            $.ajax({
                type: 'GET',
                url: "<?= Url::to(['data/search_customer_data']) ?>",
                data: {q: val},
                success: function (data, textStatus, jqXHR) {
                    //                alert(data);
                    if (data != 0) {
                        var jsonDecodedResponse = JSON.parse(data);
                        //                    alert(jsonDecodedResponse['customer_insuered_name']);
                        //                    $('#idHiddenCallerNumber').val(jsonDecodedResponse['record_id']);
                        $('#idCustomerName').text(jsonDecodedResponse['customer_insuered_name']);
//                        $('#idCustomerDOB').text(jsonDecodedResponse['customer_dob']);
//                        $('#idCustomerGender').text(jsonDecodedResponse['customer_gender']);
                        $('#idCustomerAddress').text(jsonDecodedResponse['customer_address']);
//                        $('#idCustomerEmail').text(jsonDecodedResponse['customer_email']);
                        $('#idCustomerContact').text(jsonDecodedResponse['customer_contact']);

                        //                    var callRecordsObject = jsonDecodedResponse['callRecords'];
                        //                    if (callRecordsObject.length > 0) {
                        //                        populatePreviousCallsTable(callRecordsObject);
                        //                    }

                        $("#loadingGif").css("display", "none");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //                alert(jqXHR.responseText);
                }
            });
        }




    </script>

    <?php if (!isset($_GET['agentmode'])) { // removes this block of code if agent mode get is present ?> 

        <script>
            // Common Interval to invoke mandatory AJAX functions
            $(document).ready(function () {
                //        var getOnlineAgentsInterval = setInterval(getOnlineUsersAJAX(), 3000);
                setInterval(function () {
                    getOnlineUsersAJAX();
                    //                    showAgentsResponsesAJAX();
                    //            alert("Hello");
                }, 3000);

                // get new agent and help requests
                setInterval(function () {
                    getHelpRequestsAJAX()();

                }, 2000);

            });
        </script>
        <script>
            // conference call functionality
            function initiateConference(otherPhoneNumber) {
                //document.getElementById('webphoneframe').contentWindow.initiateApiCall("*92" + otherPhoneNumber);
                document.getElementById('webphoneframe').contentWindow.addToConference(otherPhoneNumber);
            }

            function removeFromConference(otherPhoneNumber) {
                document.getElementById('webphoneframe').contentWindow.removeConference(otherPhoneNumber);
            }
            function callAnswered() {

            }

            // transfer call functionality
            function initiatetransfer(number) {
                document.getElementById('webphoneframe').contentWindow.transfer(number);
            }

            /**
             * <b></b>
             * <p></p>
             * @param {type} toAgentExtention
             * @returns {undefined}
             * @author Sandun
             * @since 2017-08-04
             */
            function inviteForConferenceAJAX(toAgentExtention) {
                $.ajax({
                    type: 'GET',
                    url: "<?= Url::to(['agent/sendconferencecallinvitation']) ?>",
                    data: {to: toAgentExtention},
                    success: function (data, textStatus, jqXHR) {
                        //                alert(data);
                        swal({
                            title: 'Invited!',
                            type: "success",
                            timer: 1000
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //                alert(jqXHR.responseText);
                    }
                });
            }
        </script>
        <script>
            // for help requests functionality
            //            if (typeof (EventSource) !== "undefined") {
            //                var source = new EventSource("<? = Url::to(['events/requests']) ?>");
            //                source.onmessage = function (event) {
            //
            //
            //                };
            //            } else {
            //                document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
            //            }


            function getHelpRequestsAJAX() {
                $.ajax({
                    url: "<?= Url::to(['events/requests']) ?>",
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        console.log("help requests success timestamp " + Date.now() + " : " + number);
                        number++;
                        if (data != "data:-1") {
                            console.log("help requests data fetch timestamp " + Date.now() + " : " + number);
                            getHelpRequests(data);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("help requests fail timestamp " + Date.now() + " : " + number);
                        number++;
                    }
                });
            }

            //            function clearHelpAndAgentRequests() {
            //                $("#helpRequestsDisplayDiv").html("");
            //                $("#agentRequestsDisplayDiv").html("");
            //                $("#agentRequestsDisplayDiv").html("");
            //            }


            function getHelpRequests(data) {
                var eventData = data;
                var arr = eventData.split(',');
                if (arr[0] != -1) {
                    if (arr[0] != '0')
                        showNewHelpRequest(arr[0], arr[1], arr[2]);
                    if (arr[3] != '0')
                        showNewAgentRequest(arr[3], arr[4], arr[5]);
                } else {

                }
            }



            function showNoHelpRequestsMessage() {
                var noHelpMessageDiv = "<div class='list-group-item' style='border: #fff' id='noHelpRequestsDiv'><span style='font-weight: bold'>No help requests</span> </div>";
                $("#helpRequestsDisplayDiv").html("");
                $("#helpRequestsDisplayDiv").append(noHelpMessageDiv);
                $("#helpRequestsCounter").html(0);
            }

            function showNewHelpRequest(id, agentName, dateTime) {
                //                var newHelpRequestDiv = "<div class='list-group-item helpRequests'><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: blueviolet'>Need a help.</span> "+dateTime+"</div>";
                var newHelpRequestDiv = "<div class='list-group-item helpRequests' id='h" + agentName + "'><span  style='font-weight: bold'>" + agentName + "</span><span class='badge'> <a style='color: red' href='#' id='" + agentName + "' onclick='removeAgentHelpNotification(this.id)' class='glyphicon glyphicon-remove'></a></span><br><span style='color: blueviolet'>Need a help.</span> <br> <span style='font-size: 80%; color: #10297d'>" + dateTime + "</span></div>";
                if ($(".helpRequests").length > 0) {
                    $("#helpRequestsDisplayDiv").prepend(newHelpRequestDiv);
                    incrementHelpRequestsCount();
                } else {
                    $("#helpRequestsDisplayDiv").html("");
                    $("#helpRequestsDisplayDiv").prepend(newHelpRequestDiv);
                    incrementHelpRequestsCount();
                }
            }
            function showNewAgentRequest(id, agentName, request) {

                var newAgentRequestDiv = '';
                if (request == 'lunch') {

                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green' id='" + id + "'  class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: blueviolet'>Lunch break</span> </div>";
                    //                    console.log(newAgentRequestDiv);
                } else if (request == 'short') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green'  id='" + id + "' class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: orange'>Short break</span> </div>";
                } else if (request == 'sick') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><input type='number' style='width:45px;color: black;' id='T" + id + "'  maxlength='1'><a style='color: green'  id='" + id + "' class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,1)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #42b0f4'>Sick break</span> </div>";
                } else if (request == 'other') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green'  id='" + id + "'   class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #000cc'>Other break</span> </div>";
                } else if (request == 'meeting') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><input type='number' style='width:45px;color: black;' id='T" + id + "'  maxlength='1'><a style='color: green'  id='" + id + "'   class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,1)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #f44180'>Meeting / training break</span> </div>";
                }

                if ($(".agentRequests").length > 0) {

                    $("#agentRequestsDisplayDiv").prepend(newAgentRequestDiv);
                    incrementAgentRequestsCount();

                } else {

                    $("#agentRequestsDisplayDiv").html("");
                    $("#agentRequestsDisplayDiv").prepend(newAgentRequestDiv);
                    incrementAgentRequestsCount();
                }
            }
            function showNoAgentRequestsMessage() {
                var noAgentMessageDiv = "<div class='list-group-item' style='border: #fff' id='noAgentRequestsDiv'><span style='font - weight: bold'>No agent break requests</span></div>";
                $("#agentRequestsDisplayDiv").html("");
                $("#agentRequestsDisplayDiv").append(noAgentMessageDiv);
                $("#agentRequestsCounter").html(0);
            }

            function decrementHelpRequestsCount() {
                var helpRequestsCount = $("#helpRequestsCounter").html();
                helpRequestsCount--;
                $("#helpRequestsCounter").html(helpRequestsCount);
                if (helpRequestsCount == 0) {
                    showNoHelpRequestsMessage();
                }
            }

            function incrementHelpRequestsCount() {
                var helpRequestsCount = $("#helpRequestsCounter").html();
                helpRequestsCount++;
                $("#helpRequestsCounter").html(helpRequestsCount);
            }

            function incrementAgentRequestsCount() {
                var requestsCount = $("#agentRequestsCounter").html();
                requestsCount++;
                $("#agentRequestsCounter").html(requestsCount);
            }

            function decrementAgentRequestsCount() {
                var requestsCount = $("#agentRequestsCounter").html();
                requestsCount--;
                $("#agentRequestsCounter").html(requestsCount);
                if (requestsCount == 0) {
                    showNoAgentRequestsMessage();
                }
            }

            // This function will be called when supervisor accept user request
            function acceptClick(elementId, isSick) {
                agentRequestResponse(elementId, 'approved', isSick);
            }
            // This function will be called when supervisor denied user request
            function deniedClick(elementId) {
                agentRequestResponse(elementId, 'denied', '0');
                //                removeAgentRequestNotification(elementId);
                //                alert(elementId);

            }
            function removeAgentRequestNotification(elementId) {
                var id = '#e' + elementId;
                $(id).remove();
                decrementAgentRequestsCount();
                if ($(".agentRequests").length == 0) {
                    showNoAgentRequestsMessage();
                }
            }
            function removeAgentHelpNotification(elementId) {
                var id = '#h' + elementId;
                $(id).remove();
                decrementHelpRequestsCount();
                if ($(".helpRequests").length == 0) {
                    showNoHelpRequestsMessage();
                }

            }
            function agentRequestResponse(id, response, isSick) {
                //                var id = $(this).attr('id');
                var execute = '1';
                var timeSlot = 0;
                var approvedTime = '#T' + id;
                if (isSick == '1') {
                    $(approvedTime).val();
                    timeSlot = $(approvedTime).val();
                    if (timeSlot == '')
                        execute = '0';
                }
                var url = "<?= Url::to(['supevisor/agentresponse']) ?>";
                if (execute == '1') {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {q: id, res: response, time: timeSlot},
                        success: function (data)
                        {
                            removeAgentRequestNotification(id);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            //                            alert(xhr.responseText);
                            //                            alert(xhr.status);
                            //                            alert(thrownError);
                            //                            alert(xhr.responseText);
                        }
                    });
                }
            }
        </script>
    <?php } ?>