<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of help_requests
 *
 * @author Sandun
 * @since 2017-07-12
 */
class Help_requests extends ActiveRecord{
    
    /**
     * <b>Saves the help request</b>
     * <p>This function saves the agentId and the datetime it received to the DB</p>
     * 
     * @author Sandun
     * @since 2017-07-13
     * 
     * @param int $agentId
     * @return bool
     */
    public static function saveRequest($agentId){
        $helpRequest = new Help_requests();
        $helpRequest->agentId = $agentId;
        $helpRequest->datetime = date("Y-m-d H:m:s");
        $helpRequest->consumed = 0;
        return $helpRequest->insert();
    }
    
    /**
     * <b>Sets the consumed to 1</b>
     * <p>This function sets the consumed value of the help request identified by the passing parameter to 1 to indicates its consumed</p>
     * 
     * @author Sandun
     * @since 2017-07-13
     * 
     * @param int $helpRequestId
     * @return bool
     */
    public static function markConsumed($helpRequestId){
        $helpRequest = new Help_requests();
        return $helpRequest->updateAll(['consumed' => 1], "id = $helpRequestId");
    }
    
    /**
     * <b>Gets all unconsumed help requests</b>
     * <p>this function return all unconsumed help requests from the DB table</p>
     * 
     * @return array
     * @author Sandun
     * @since 2017-07-14
     * 
     * @modified Sandun 2017-12-18
     * @description returns All agents help requests for admin.
     * Returns the helps requests of the assigned agents in the queue belongs to the supervisor / super user
     */
    public static function getUnconsumedHelpRequests(){
        if(Yii::$app->session->get("user_role") == "1"){
            // user is an admin
            $helpRequestQuery = Help_requests::find();
            return $helpRequestQuery->where("consumed = 0")
                                    ->all();
        }else{
            // user is not an admin
            $userIdsInQueue = call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId"));
            $helpRequestQuery = Help_requests::find();
            return $helpRequestQuery->where("consumed = 0")
                                    ->andWhere(['in', 'agentid', $userIdsInQueue])
                                    ->all();            
            
        }
    }
}
