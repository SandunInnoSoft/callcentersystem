<?php


namespace app\models;
use yii;
use yii\db\ActiveRecord;

/**
 * This model class interacts with extension_queue table in DB1
 *
 * @author Sandun
 * @since 2017-12-12
 */
class Extension_queue extends ActiveRecord{
    
    /**
     * <b>Returns all assigned supervisor ids</b>
     * @return array
     * 
     * @since 2017-12-12
     * @author Sandun
     */
    public static function getAllAssignedSupervisorIds(){
        return Extension_queue::find()
                ->select("supervisor_id")
                ->all();
        
    }
    
    public static function insertNewExtensionQueue($queueName, $supervisorId){
        $newExtensionQueue = new Extension_queue();
        $newExtensionQueue->name = $queueName;
        $newExtensionQueue->supervisor_id = $supervisorId;
        $newExtensionQueue->insert();
        return $newExtensionQueue->getPrimaryKey();
    }
    
    public static function getExtensionQueueIdOfTheSupervisor($supervisorId){
        $extensionQueueData = Extension_queue::find()
                ->select("id")
                ->where("supervisor_id = $supervisorId")
                ->one();
        
        if($extensionQueueData['id']){
            return $extensionQueueData['id'];
        }else{
            return NULL;
        }
    }
    
    /**
     * <b>Checks if the supervisor has an extension queue assigned</b>
     * <p></p>
     * 
     * @author Sandun
     * @since 2017-12-14
     * 
     * @return queue id if assigned / FALSE if not assigned
     */
    public static function isSupervisorAssignedToAnExtensionQueue($supervisorId){
        $supervisorQueue = Extension_queue::find()
                ->where("supervisor_id = $supervisorId")
                ->one();
        
        if($supervisorQueue){
            return $supervisorQueue['id'];
        }else{
            return false;
        }
    }
    
    /**
     * <b>Get all available queues data</b>
     * <p>This function returns data of all available queues</p>
     * 
     * @return array all available queues
     * 
     * @since 2017-12-15
     * @author Sandun
     */
    public static function getAllAvailableQueuesData(){
        return Extension_queue::find()
                ->all();
    }
    
    /**
     * 
     * @param type $queueId
     * @return type
     * 
     * @since 2018-1-2
     * @author Sandun
     */
    public function getExtensionQueueNameByQueueId($queueId){
        $queueName = Extension_queue::find()
                ->select("name")
                ->where("id = $queueId")
                ->one();
        return $queueName["name"];
    }

        /**
     * <b>Get all available queues of Executive</b>
     * <p>This function returns data of all allowed queue information of logged executive user</p>
     *
     * @param  $executiveId - int - Logged executive id
     * @return array all allowed queues of selected executive
     * 
     * @since 2018-08-13
     * @author Vinothan
     */
    public function getAllAvailableQueuesDataOfSelectedExecutive($executiveId){        
        $executiveQueue = Extension_queue::find()
                ->select(['extension_queue.id' , 'extension_queue.name' , 'extension_queue.supervisor_id'])  
                ->from("extension_queue")
                ->join( "INNER JOIN", 
                    "allowed_exec_extqueues",
                    "allowed_exec_extqueues.allowed_ext_queue_id =extension_queue.id")
                ->where("allowed_exec_extqueues.user_id = $executiveId")
                ->groupby(['extension_queue.id' , 'extension_queue.name' , 'extension_queue.supervisor_id'])
                ->all();
        return $executiveQueue;
    }


        public function getAllAvailableQueuesIdOfSelectedExecutive($executiveId){        
        $executiveQueue = Extension_queue::find()
                ->select(['extension_queue.id'])  
                ->from("extension_queue")
                ->join( "INNER JOIN", 
                    "allowed_exec_extqueues",
                    "allowed_exec_extqueues.allowed_ext_queue_id =extension_queue.id")
                ->where("allowed_exec_extqueues.user_id = $executiveId")
                ->groupby(['extension_queue.id'])
                ->all();
        return $executiveQueue;
    }
    
}
